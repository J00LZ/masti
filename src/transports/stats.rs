use std::{
    io::Write,
    sync::{atomic::{AtomicU64, AtomicUsize}, Arc},
    time::Duration,
};

use serde::Serialize;

#[derive(Debug, Default, Clone)]
pub struct Stats(Arc<InnerStats>);

impl Serialize for Stats {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        self.0.serialize(serializer)
    }
}

#[derive(Debug, Default, Serialize)]
pub struct InnerStats {
    should_sent: AtomicUsize,

    bytes_sent: AtomicUsize,
    bytes_received: AtomicUsize,
    /// In Nanoseconds
    rtt_sum: AtomicU64,
    packets_sent: AtomicUsize,
    packets_received: AtomicUsize,
    rtt_count: AtomicUsize,
}

impl Stats {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn set_should_send(&self, n: usize) {
        self.0
            .should_sent
            .store(n, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn sent_packets(&self, packet_count: usize, byte_count: usize) {
        self.0
            .packets_sent
            .fetch_add(packet_count, std::sync::atomic::Ordering::SeqCst);
        self.0
            .bytes_sent
            .fetch_add(byte_count, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn bytes_received(&self, byte_count: usize) {
        self.0
            .bytes_received
            .fetch_add(byte_count, std::sync::atomic::Ordering::SeqCst);
    }

    /// rtt in nanoseconds
    pub fn rtt(&self, rtt: u64) {
        self.0
            .rtt_sum
            .fetch_add(rtt, std::sync::atomic::Ordering::Relaxed);
        self.0
            .rtt_count
            .fetch_add(1, std::sync::atomic::Ordering::Relaxed);
    }

    pub fn get_rtt(&self) -> f64 {
        self.0.rtt_sum.load(std::sync::atomic::Ordering::SeqCst) as f64
            / self.0.rtt_count.load(std::sync::atomic::Ordering::SeqCst) as f64
    }

    pub fn received_packet(&self) {
        self.0
            .packets_received
            .fetch_add(1, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn write_all(&self, w: &mut impl Write) {
        writeln!(
            w,
            "{},{},{},{},{}",
            self.0.bytes_sent.load(std::sync::atomic::Ordering::SeqCst),
            self.0
                .bytes_received
                .load(std::sync::atomic::Ordering::SeqCst),
            self.get_rtt(),
            self.0
                .packets_sent
                .load(std::sync::atomic::Ordering::SeqCst),
            self.0
                .packets_received
                .load(std::sync::atomic::Ordering::SeqCst)
        )
        .unwrap();
    }
}
