use std::time::{Duration};

use serde::{Deserialize, Serialize};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct ChannelMessage {
    pub channel: u8,
    pub message: Vec<u8>,
    pub send_time: Duration,
}

impl ChannelMessage {
    pub fn new<CH: Into<u8>>(channel: CH, message: bytes::Bytes, send_time: Duration) -> Self {
        Self {
            channel: channel.into(),
            message: message.to_vec(),
            send_time,
        }
    }
}
