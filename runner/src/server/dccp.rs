use std::sync::mpsc::Receiver;

use dccp::{DCCPListener, DCCPSocket};

use crate::args::RunnerArgs;

use super::impls::ServerTransport;

pub struct DCCPTomfoolery {
    pub receiver: Receiver<DCCPSocket>,
}

impl ServerTransport for DCCPTomfoolery {
    async fn new(args: RunnerArgs) -> Self {
        let server = DCCPListener::bind(args.address, 0x3F343230).unwrap();
        let (sender, receiver) = std::sync::mpsc::channel();
        std::thread::spawn(move || loop {
            match server.accept() {
                Ok(socket) => {
                    sender.send(socket).unwrap();
                }
                Err(e) => {
                    eprintln!("Error accepting connection: {:?}", e);
                }
            }
        });
        Self { receiver }
    }
}

impl DCCPTomfoolery {
    pub fn accept(&self) -> Option<DCCPSocket> {
        self.receiver.try_recv().ok()
    }
}
