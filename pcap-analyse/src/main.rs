use std::{path::Path, process::Command, str::FromStr};

#[derive(Debug)]
enum Type {
    Tcp,
    RaknetReliable,
    RaknetUnreliable,
    Sctp,
    Dccp,
    Quic,
}

#[derive(Debug)]
enum Connection {
    Ethernet,
    Wifi,
    G5,
    G4,
    G2,
}

#[derive(Debug)]
enum Size {
    Full,
    TwoThirds,
    Third,
}

#[derive(Debug)]
struct Pcap {
    connection: Connection,
    size: Size,
    type_: Type,
}

impl FromStr for Pcap {
    type Err = ();

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        // raknet-reliable - ethernet - 13.pcapng
        let mut stuff = s.trim().trim_end_matches(".pcapng").split(" - ");
        let type_ = match stuff.next().unwrap() {
            "tcp" => Type::Tcp,
            "raknet-reliable" => Type::RaknetReliable,
            "raknet-unreliable" => Type::RaknetUnreliable,
            "quic" => Type::Quic,
            "sctp" => Type::Sctp,
            "dccp" => Type::Dccp,
            _ => panic!(),
        };
        let connection = match stuff.next().unwrap() {
            "ethernet" => Connection::Ethernet,
            "wifi" => Connection::Wifi,
            "5g" => Connection::G5,
            "4g" => Connection::G4,
            "2g" => Connection::G2,
            _ => panic!(),
        };
        let size = match stuff.next().unwrap() {
            "full" => Size::Full,
            "23" => Size::TwoThirds,
            "13" => Size::Third,
            _ => panic!(),
        };
        Ok(Pcap {
            connection,
            size,
            type_,
        })
    }
}

fn main() {
    let pcaps = std::fs::read_dir("./pcaps").unwrap();
    for pcap in pcaps {
        let entry = pcap.unwrap();
        let f: Pcap = entry.file_name().to_string_lossy().parse().unwrap();
        if matches!(f.connection, Connection::Ethernet) && matches!(f.type_, Type::Quic) {

            run_pcap(&f, &entry.path());
        }
    }
}

fn run_pcap(pcap: &Pcap, file: &Path) {
    let command = Command::new("tshark")
        .arg("-r")
        .arg(file)
        .arg("-O")
        .arg("tcp,udp,sctp,dccp")
        .arg("-T")
        .arg("fields")
        .arg("-e")
        .arg("udp.dstport")
        .arg("-e")
        .arg("tcp.dstport")
        .arg("-e")
        .arg("sctp.dstport")
        .arg("-e")
        .arg("dccp.dstport")
        .arg("-e")
        .arg("frame.len")
        .output()
        .expect("failed to execute process");
    let output = String::from_utf8_lossy(&command.stdout);
    let mut sent_bytes = 0;
    let mut recv_bytes = 0;
    for line in output.split("\n").filter(|x| !x.is_empty()) {
        let mut stuff = line.split_ascii_whitespace();
        let Some(dest) = stuff.next() else {
            eprintln!("Error parsing dest: {}", file.display());
            continue;
        };
        let Some(size) = stuff.next() else {
            eprintln!("Error parsing dest: {}", file.display());
            continue;
        };
        let size = match size.parse::<u64>() {
            Ok(r) => r,
            Err(_) => {
                eprintln!("Error parsing size: {} (in {})", size, file.display());
                continue;
            }
        };
        if dest == "5000" {
            sent_bytes += size;
        } else {
            recv_bytes += size;
        }
    }
    println!(
        "{:?},{:?},{:?},{},{}",
        pcap.type_, pcap.connection, pcap.size, sent_bytes, recv_bytes
    );
}
