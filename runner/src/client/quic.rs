use std::pin::pin;

use bytes::Bytes;
use futures::poll;
use s2n_quic::{client::Connect, stream::BidirectionalStream, Client};

use crate::args::RunnerArgs;

use super::impls::ClientTransport;

impl ClientTransport for BidirectionalStream {
    const NAME: &'static str = "quic";
    type Server = s2n_quic::Server;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        let f = server.accept();
        match poll!(pin!(f)) {
            std::task::Poll::Ready(c) => {
                let mut c = c?;
                let s = c.accept_bidirectional_stream().await.ok()??;
                println!("Client connected");
                Some(s)
            }
            std::task::Poll::Pending => None,
        }
    }

    async fn new(args: RunnerArgs) -> Self {
        let client = Client::builder()
            .with_tls(args.cert_dir.join("domain.crt").as_path())
            .unwrap()
            .with_io("0.0.0.0:0")
            .unwrap()
            .start()
            .unwrap();

        let socket = Connect::new(args.address).with_server_name("localhost");
        let mut connection = client.connect(socket).await.unwrap();
        let s = connection.open_bidirectional_stream().await.unwrap();
        s
    }

    async fn write_message(&mut self, bytes: Bytes) -> std::io::Result<()> {
        self.send(bytes).await?;

        Ok(())
    }

    async fn read_bytes(&mut self) -> std::io::Result<Bytes> {
        let n = self.receive().await?.ok_or_else(|| {
            std::io::Error::new(std::io::ErrorKind::ConnectionAborted, "Connection closed")
        })?;
        Ok(n)
    }
}
