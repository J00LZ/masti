use std::{io::Result as IoResult, pin::pin, task::Poll};

use bytes::Bytes;
use futures::poll;
use tokio::{io::{AsyncReadExt, AsyncWriteExt}, net::{TcpListener, TcpStream}};

use crate::args::RunnerArgs;

use super::impls::ClientTransport;

impl ClientTransport for TcpStream {
    const NAME: &'static str = "tcp";
    type Server = TcpListener;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        match poll!(pin!(server.accept())) {
            Poll::Ready(c) => {
                let (stream, _) = c.ok()?;
                Some(stream)
            }
            Poll::Pending => None,
        }
    }

    async fn new(args: RunnerArgs) -> Self {
        TcpStream::connect(args.address).await.unwrap()
    }

    async fn write_message(&mut self, bytes: Bytes) -> IoResult<()> {
        self.write_all(&bytes).await
    }

    async fn read_bytes(&mut self) -> IoResult<Bytes> {
        let mut bytes = [0; 1500];
        let r = self.read(&mut bytes).await?;
        Ok(Bytes::copy_from_slice(&bytes[..r]))
    }
}
