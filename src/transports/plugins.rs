use std::marker::PhantomData;

use bevy::{
    app::{App, AppExit, Plugin, PostUpdate, PreUpdate},
    ecs::{
        event::{EventReader, EventWriter, Events},
        schedule::{common_conditions::resource_exists, IntoSystemConfigs},
        system::{Res, ResMut, Resource},
    },
    time::Time,
};
use bevy_renet::{renet::ServerEvent, RenetReceive, RenetSend};

use crate::transports::TransportError;

pub mod transport;

use transport::{ClientTransport, Transport};

use super::{Client, Server};

pub struct TransportLayerServerPlugin<Transport, Server>(PhantomData<(Transport, Server)>);

impl<Transport, Server> Default for TransportLayerServerPlugin<Transport, Server> {
    fn default() -> Self {
        Self(PhantomData)
    }
}

pub struct TransportLayerClientPlugin<ClientTransport, Client>(
    PhantomData<(ClientTransport, Client)>,
);

impl<Transport, Server> Default for TransportLayerClientPlugin<Transport, Server> {
    fn default() -> Self {
        Self(PhantomData)
    }
}

impl<T: Transport<Server = S>, S: Server> Plugin for TransportLayerServerPlugin<T, S> {
    fn build(&self, app: &mut App) {
        app.add_event::<TransportError>();

        app.add_systems(
            PreUpdate,
            Self::update_system
                .in_set(RenetReceive)
                .run_if(resource_exists::<T>)
                .run_if(resource_exists::<S>)
                .after(ServerPlugin::<S>::update_system)
                .before(ServerPlugin::<S>::emit_server_events_system),
        );

        app.add_systems(
            PostUpdate,
            (
                Self::send_packets.in_set(RenetSend),
                Self::disconnect_on_exit,
            )
                .run_if(resource_exists::<T>)
                .run_if(resource_exists::<S>),
        );
    }
}

impl<T: Transport<Server = S>, S: Resource> TransportLayerServerPlugin<T, S> {
    pub fn update_system(
        mut transport: ResMut<T>,
        mut server: ResMut<S>,
        time: Res<Time>,
        mut transport_errors: EventWriter<TransportError>,
    ) {
        if let Err(e) = transport.update(time.delta(), &mut server) {
            transport_errors.send(e);
        }
    }

    pub fn send_packets(mut transport: ResMut<T>, mut server: ResMut<S>) {
        transport.send_packets(&mut server);
    }

    pub fn disconnect_on_exit(
        exit: EventReader<AppExit>,
        mut transport: ResMut<T>,
        mut server: ResMut<S>,
    ) {
        if !exit.is_empty() {
            transport.disconnect_all(&mut server);
        }
    }
}

impl<CT: ClientTransport<Client = C>, C: Client> Plugin for TransportLayerClientPlugin<CT, C> {
    fn build(&self, app: &mut App) {
        app.add_event::<TransportError>();

        app.add_systems(
            PreUpdate,
            Self::update_system
                .in_set(RenetReceive)
                .run_if(resource_exists::<CT>)
                .run_if(resource_exists::<C>)
                .after(ClientPlugin::<C>::update_system),
        );
        app.add_systems(
            PostUpdate,
            (
                Self::send_packets.in_set(RenetSend),
                Self::disconnect_on_exit,
            )
                .run_if(resource_exists::<CT>)
                .run_if(resource_exists::<C>),
        );
    }
}

impl<CT: ClientTransport<Client = C>, C: Client> TransportLayerClientPlugin<CT, C> {
    pub fn update_system(
        mut transport: ResMut<CT>,
        mut client: ResMut<C>,
        time: Res<Time>,
        mut transport_errors: EventWriter<TransportError>,
    ) {
        if let Err(e) = transport.update(time.delta(), &mut client) {
            transport_errors.send(e);
        }
    }

    pub fn send_packets(
        mut transport: ResMut<CT>,
        mut client: ResMut<C>,
        mut transport_errors: EventWriter<TransportError>,
    ) {
        if let Err(e) = transport.send_packets(&mut client) {
            transport_errors.send(e);
        }
    }

    pub fn disconnect_on_exit(exit: EventReader<AppExit>, mut transport: ResMut<CT>) {
        if !exit.is_empty() {
            transport.disconnect();
        }
    }
}

pub struct ServerPlugin<S>(PhantomData<S>);

pub struct ClientPlugin<C>(PhantomData<C>);

impl<S: Server> Plugin for ServerPlugin<S> {
    fn build(&self, app: &mut App) {
        app.init_resource::<Events<ServerEvent>>();
        app.add_systems(PreUpdate, Self::update_system.run_if(resource_exists::<S>));
        app.add_systems(
            PreUpdate,
            Self::emit_server_events_system
                .in_set(RenetReceive)
                .run_if(resource_exists::<S>)
                .after(Self::update_system),
        );
    }
}

impl<S: Server> ServerPlugin<S> {
    pub fn update_system(mut server: ResMut<S>, time: Res<Time>) {
        server.update(time.delta());
    }

    pub fn emit_server_events_system(
        mut server: ResMut<S>,
        mut server_events: EventWriter<ServerEvent>,
    ) {
        while let Some(event) = server.get_event() {
            server_events.send(event);
        }
    }
}

impl<S> Default for ServerPlugin<S> {
    fn default() -> Self {
        Self(PhantomData)
    }
}

impl<C: Client> Plugin for ClientPlugin<C> {
    fn build(&self, app: &mut App) {
        app.add_systems(PreUpdate, Self::update_system.run_if(resource_exists::<C>));
    }
}

impl<C: Client> ClientPlugin<C> {
    pub fn update_system(mut client: ResMut<C>, time: Res<Time>) {
        client.update(time.delta());
    }
}

impl<C> Default for ClientPlugin<C> {
    fn default() -> Self {
        Self(PhantomData)
    }
}
