use crate::args::RunnerArgs;

use super::impls::ServerTransport;

impl ServerTransport for sctp_rs::Listener {
    async fn new(args: RunnerArgs) -> Self {
        let r = sctp_rs::Socket::new_v4(sctp_rs::SocketToAssociation::OneToOne).unwrap();
        r.bind(args.address).unwrap();
        r.listen(100).unwrap()
    }
}
