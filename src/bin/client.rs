use std::{
    collections::HashMap,
    fs::{create_dir_all, File, OpenOptions},
    io::Write,
    net::ToSocketAddrs,
    process::exit,
    time::{Duration, Instant, SystemTime},
};

use bevy::{diagnostic::LogDiagnosticsPlugin, prelude::*, window::PrimaryWindow};
use bevy_egui::{EguiContexts, EguiPlugin};

use demo_bevy::{
    setup_level,
    transports::{
        self, headless_traits::HeadlessClient, quic::client::QuicClient, renet::client::RenetClientShim, tcp::client::TcpClient, Client, ClientId
    },
    ClientChannel, NetworkedEntities, PlayerCommand, PlayerInput, SelectedClient, ServerChannel,
    ServerMessages,
};
use renet_visualizer::{RenetClientVisualizer, RenetVisualizerStyle};
use serde::{Deserialize, Serialize};
use smooth_bevy_cameras::{LookTransform, LookTransformBundle, LookTransformPlugin, Smoother};
use tokio::time::sleep;

#[derive(Component)]
struct ControlledPlayer;

#[derive(Default, Resource)]
struct NetworkMapping(HashMap<Entity, Entity>);

#[derive(Debug)]
struct PlayerInfo {
    client_entity: Entity,
    server_entity: Entity,
}

#[derive(Debug, Default, Resource)]
struct ClientLobby {
    players: HashMap<ClientId, PlayerInfo>,
}

#[derive(Serialize, Deserialize)]
enum Foo {
    PlayerInput(PlayerInput),
    PlayerCommand(PlayerCommand),
}

#[derive(Serialize, Deserialize)]
struct Bar {
    pub foo: Foo,
    pub time: Duration,
}

/// Selects which transport protocol is being used

#[derive(Resource)]
struct FileThing {
    file: File,
}
impl FileThing {
    fn new() -> Self {
        let now = SystemTime::now();
        create_dir_all("logs").unwrap();
        let mut file = File::create(format!(
            "logs/{}.json",
            now.duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        ))
        .unwrap();
        writeln!(file, "[").unwrap();
        Self { file }
    }
}

impl Drop for FileThing {
    fn drop(&mut self) {
        let elapsed = Duration::from_secs(500);
        let input = PlayerInput::default();
        let bar = Bar {
            foo: Foo::PlayerInput(input),
            time: elapsed,
        };
        serde_json::to_writer(&self.file, &bar).unwrap();
        writeln!(self.file).unwrap();
        write!(self.file, "]").unwrap();
    }
}

/// Run the client headless
async fn headless(log: &str) -> ! {
    // Read log file
    let file = File::open(log).unwrap();
    let mut inputs: Vec<Bar> = serde_json::from_reader(file).unwrap();
    inputs.reverse(); // Newest at the back so we can pop
                      // remove the last input if it'll take too long to get there
    if inputs[0].time > Duration::from_secs(100) {
        inputs.remove(0);
    }
    inputs.reverse();
    let client_id = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64;
    // Initialize tcp client
    // let mut client = SelectedClient::init(client_id); // tcp client gets id from server
    let mut c = HC::<TcpClient>::new(ClientId(client_id), "127.0.0.1:5000").await;
    println!("Connected!");

    c.client.stats.set_should_send(inputs.len());

    // Fake game loop
    c.run_inputs(inputs).await;

    let stats = c.client.stats;
    println!("Stats: {:?}", stats);
    let mut f = OpenOptions::new()
        .append(true)
        .create(true)
        .open("./out.csv")
        .unwrap();
    stats.write_all(&mut f);

    exit(0)
}

struct HC<C> {
    client: C,
}

impl<C: HeadlessClient + Send> HC<C> {
    async fn new(id: ClientId, addr: impl ToSocketAddrs) -> Self {
        Self {
            client: C::new(id, addr).await,
        }
    }

    async fn run_inputs(&mut self, inputs: impl IntoIterator<Item = Bar>) {
        // Calculate the duration for 1/120th of a second35118
        let frame_duration = Duration::from_secs_f32(1.0 / 60.0);
        let mut inputs = inputs.into_iter().peekable();
        // Start an infinite loop
        let start_time = Instant::now();
        let mut last_frame_time = start_time;
        loop {
            // Calculate how much time we need to sleep to maintain 60Hz
            let elapsed = last_frame_time.elapsed();
            if elapsed < frame_duration {
                tokio::time::sleep(frame_duration - elapsed).await;
            } else {
                eprintln!("Could not keep up")
            }

            // Update the time of the last frame
            last_frame_time = Instant::now();
            // Updat the client
            self.client.update(elapsed).await;
            self.client.update_transport(elapsed).await;

            if self.client.is_connected() {
                while let Some(msg) = self
                    .client
                    .receive_message(ServerChannel::NetworkedEntities)
                    .await
                {
                    // println!("Received networked entities: {:?}", msg);
                }
                while let Some(msg) = self
                    .client
                    .receive_message(ServerChannel::ServerMessages)
                    .await
                {
                    // println!("Received server message: {:?}", msg);
                }

                // Send message if necessary
                while let Some(last) = inputs.peek() {
                    if start_time.elapsed() >= last.time {
                        let last = inputs.next().unwrap();
                        // send the packet
                        match last.foo {
                            Foo::PlayerInput(msg) => {
                                // println!("Sending input: {:?}", msg);
                                let bytes = bincode::serialize(&msg).unwrap();
                                self.client.send_message(ClientChannel::Input as u8, bytes).await;
                            }
                            Foo::PlayerCommand(msg) => {
                                let bytes = bincode::serialize(&msg).unwrap();
                                self.client
                                    .send_message(ClientChannel::Command as u8, bytes)
                                    .await;
                            }
                        }
                    } else {
                        break;
                    }
                }
                self.client.send_packets().await;
            }

            // Exit when done
            if inputs.peek().is_none() {
                self.client.send_packets().await;

                sleep(Duration::from_millis(100)).await;

                while let Some(msg) = self
                    .client
                    .receive_message(ServerChannel::NetworkedEntities)
                    .await
                {
                    // println!("Received networked entities: {:?}", msg);
                }
                while let Some(msg) = self
                    .client
                    .receive_message(ServerChannel::ServerMessages)
                    .await
                {
                    // println!("Received server message: {:?}", msg);
                }

                break;
            }
        }
    }
}

#[tokio::main]
async fn main() {
    headless("./logs/1711530329.json").await;

    let mut app = App::new();
    app.add_plugins(DefaultPlugins);
    app.add_plugins(LookTransformPlugin);
    // app.add_plugins(FrameTimeDiagnosticsPlugin);
    app.add_plugins(LogDiagnosticsPlugin::default());
    app.add_plugins(EguiPlugin);

    // renet
    let client_id = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap()
        .as_millis() as u64;
    println!("Starting client part");
    SelectedClient::init_bevy(&mut app, client_id);
    app.insert_resource(ClientId(client_id));

    app.insert_resource(FileThing::new());

    app.add_event::<PlayerCommand>();

    app.insert_resource(ClientLobby::default());
    app.insert_resource(PlayerInput::default());
    app.insert_resource(NetworkMapping::default());

    app.add_systems(Update, (player_input, camera_follow, update_target_system));
    app.add_systems(
        Update,
        (
            client_send_input::<SelectedClient>,
            client_send_player_commands::<SelectedClient>,
            client_sync_players::<SelectedClient>,
        )
            // .in_set(Connected),
            .run_if(transports::client_connected::<SelectedClient>),
    );

    app.insert_resource(RenetClientVisualizer::<200>::new(
        RenetVisualizerStyle::default(),
    ));

    app.add_systems(Startup, (setup_level, setup_camera, setup_target));
    app.add_systems(Update, update_visulizer_system);

    app.run();
}

fn update_visulizer_system(
    mut egui_contexts: EguiContexts,
    mut visualizer: ResMut<RenetClientVisualizer<200>>,
    client: Res<SelectedClient>,
    mut show_visualizer: Local<bool>,
    keyboard_input: Res<ButtonInput<KeyCode>>,
) {
    visualizer.add_network_info(client.network_info());
    if keyboard_input.just_pressed(KeyCode::F1) {
        *show_visualizer = !*show_visualizer;
    }
    if *show_visualizer {
        visualizer.show_window(egui_contexts.ctx_mut());
    }
}

fn player_input(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut player_input: ResMut<PlayerInput>,
    mouse_button_input: Res<ButtonInput<MouseButton>>,
    target_query: Query<&Transform, With<Target>>,
    mut player_commands: EventWriter<PlayerCommand>,
) {
    player_input.left =
        keyboard_input.pressed(KeyCode::KeyA) || keyboard_input.pressed(KeyCode::ArrowLeft);
    player_input.right =
        keyboard_input.pressed(KeyCode::KeyD) || keyboard_input.pressed(KeyCode::ArrowRight);
    player_input.up =
        keyboard_input.pressed(KeyCode::KeyW) || keyboard_input.pressed(KeyCode::ArrowUp);
    player_input.down =
        keyboard_input.pressed(KeyCode::KeyS) || keyboard_input.pressed(KeyCode::ArrowDown);

    if mouse_button_input.just_pressed(MouseButton::Left) {
        let target_transform = target_query.single();
        player_commands.send(PlayerCommand::BasicAttack {
            cast_at: target_transform.translation,
        });
    }
}

fn client_send_input<C: Client>(
    player_input: Res<PlayerInput>,
    mut client: ResMut<C>,
    time: Res<Time>,
    mut file_thing: ResMut<FileThing>,
) {
    {
        let elapsed = time.elapsed();
        let bar = Bar {
            foo: Foo::PlayerInput(*player_input),
            time: elapsed,
        };
        serde_json::to_writer(&file_thing.file, &bar).unwrap();
        write!(file_thing.file, ",\n").unwrap();
    }
    let input_message = bincode::serialize(&*player_input).unwrap();
    client.send_message(ClientChannel::Input, input_message);
}

fn client_send_player_commands<C: Client>(
    mut player_commands: EventReader<PlayerCommand>,
    mut client: ResMut<C>,
    time: Res<Time>,
    mut file_thing: ResMut<FileThing>,
) {
    for command in player_commands.read() {
        {
            let elapsed = time.elapsed();
            let bar = Bar {
                foo: Foo::PlayerCommand(*command),
                time: elapsed,
            };
            serde_json::to_writer(&file_thing.file, &bar).unwrap();
            writeln!(file_thing.file, ",").unwrap();
        }
        let command_message = bincode::serialize(command).unwrap();
        client.send_message(ClientChannel::Command, command_message);
    }
}

fn client_sync_players<C: Client>(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut client: ResMut<C>,
    client_id: Res<ClientId>,
    mut lobby: ResMut<ClientLobby>,
    mut network_mapping: ResMut<NetworkMapping>,
) {
    let client_id = client_id.0;
    while let Some(message) = client.receive_message(ServerChannel::ServerMessages) {
        let server_message = bincode::deserialize(&message).unwrap();
        println!("Received message: {:?}", server_message);
        match server_message {
            ServerMessages::PlayerCreate {
                id,
                translation,
                entity,
            } => {
                println!("Player {} connected.", id.0);
                let mut client_entity = commands.spawn(PbrBundle {
                    mesh: meshes.add(Mesh::from(Capsule3d::default())),
                    material: materials.add(Color::rgb(0.8, 0.7, 0.6)),
                    transform: Transform::from_xyz(translation[0], translation[1], translation[2]),
                    ..Default::default()
                });

                if client_id == id.0 {
                    client_entity.insert(ControlledPlayer);
                }

                let player_info = PlayerInfo {
                    server_entity: entity,
                    client_entity: client_entity.id(),
                };
                lobby.players.insert(id, player_info);
                network_mapping.0.insert(entity, client_entity.id());
            }
            ServerMessages::PlayerRemove { id } => {
                println!("Player {} disconnected.", id.0);
                if let Some(PlayerInfo {
                    server_entity,
                    client_entity,
                }) = lobby.players.remove(&id)
                {
                    commands.entity(client_entity).despawn();
                    network_mapping.0.remove(&server_entity);
                }
            }
            ServerMessages::SpawnProjectile {
                entity,
                translation,
            } => {
                let projectile_entity = commands.spawn(PbrBundle {
                    mesh: meshes.add(Mesh::from(Sphere::new(0.1))),
                    material: materials.add(Color::rgb(1.0, 0.0, 0.0)),
                    transform: Transform::from_translation(translation.into()),
                    ..Default::default()
                });
                network_mapping.0.insert(entity, projectile_entity.id());
            }
            ServerMessages::DespawnProjectile { entity } => {
                if let Some(entity) = network_mapping.0.remove(&entity) {
                    commands.entity(entity).despawn();
                }
            }
            ServerMessages::Helo { id } => {
                commands.remove_resource::<ClientId>();
                commands.insert_resource(id);
                client.set_client_id(id);
            }
        }
    }

    while let Some(message) = client.receive_message(ServerChannel::NetworkedEntities) {
        let networked_entities: NetworkedEntities = bincode::deserialize(&message).unwrap();

        for i in 0..networked_entities.entities.len() {
            if let Some(entity) = network_mapping.0.get(&networked_entities.entities[i]) {
                let translation = networked_entities.translations[i].into();
                let transform = Transform {
                    translation,
                    ..Default::default()
                };
                commands.entity(*entity).insert(transform);
            }
        }
    }
}

#[derive(Component)]
struct Target;

fn update_target_system(
    primary_window: Query<&Window, With<PrimaryWindow>>,
    mut target_query: Query<&mut Transform, With<Target>>,
    camera_query: Query<(&Camera, &GlobalTransform)>,
) {
    let (camera, camera_transform) = camera_query.single();
    let mut target_transform = target_query.single_mut();
    if let Some(cursor_pos) = primary_window.single().cursor_position() {
        if let Some(ray) = camera.viewport_to_world(camera_transform, cursor_pos) {
            if let Some(distance) = ray.intersect_plane(Vec3::Y, Plane3d::new(Vec3::Y)) {
                target_transform.translation = ray.direction * distance + ray.origin;
            }
        }
    }
}

fn setup_camera(mut commands: Commands) {
    commands
        .spawn(LookTransformBundle {
            transform: LookTransform {
                eye: Vec3::new(0.0, 8., 2.5),
                target: Vec3::new(0.0, 0.5, 0.0),
                up: Vec3::Y,
            },
            smoother: Smoother::new(0.9),
        })
        .insert(Camera3dBundle {
            transform: Transform::from_xyz(0., 8.0, 2.5)
                .looking_at(Vec3::new(0.0, 0.5, 0.0), Vec3::Y),
            ..default()
        });
}

fn setup_target(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    commands
        .spawn(PbrBundle {
            mesh: meshes.add(Mesh::from(Sphere::new(0.1))),
            material: materials.add(Color::rgb(1.0, 0.0, 0.0)),
            transform: Transform::from_xyz(0.0, 0., 0.0),
            ..Default::default()
        })
        .insert(Target);
}

fn camera_follow(
    mut camera_query: Query<&mut LookTransform, (With<Camera>, Without<ControlledPlayer>)>,
    player_query: Query<&Transform, With<ControlledPlayer>>,
) {
    let mut cam_transform = camera_query.single_mut();
    if let Ok(player_transform) = player_query.get_single() {
        cam_transform.eye.x = player_transform.translation.x;
        cam_transform.eye.z = player_transform.translation.z + 2.5;
        cam_transform.target = player_transform.translation;
    }
}
