use s2n_quic::Server;

use crate::args::RunnerArgs;

use super::impls::ServerTransport;

impl ServerTransport for Server {
    async fn new(args: RunnerArgs) -> Self {
        let socket = Server::builder()
            .with_tls((
                args.cert_dir.join("domain.crt").as_path(),
                args.cert_dir.join("domain.key").as_path(),
            ))
            .unwrap()
            .with_io(args.address)
            .unwrap()
            .start();
        let socket = match socket {
            Ok(s) => s,
            Err(e) => {
                panic!("Error starting server: {}", e);
            }
        };
        socket
    }
}
