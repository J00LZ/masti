use std::{future::Future, net::ToSocketAddrs};

use bytes::Bytes;

use crate::transports::ServerEvent;

use super::ClientId;

pub trait HeadlessServer {
    fn new(addr: impl ToSocketAddrs) -> impl Future<Output = Self>
    where
        Self: Send + Sized;
    fn broadcast_message<B: Into<Bytes>>(
        &mut self,
        channel_id: u8,
        message: B,
    ) -> impl Future<Output = ()>;
    fn receive_message<I: Into<u8>>(
        &mut self,
        client_id: ClientId,
        channel_id: I,
    ) -> impl Future<Output = Option<Bytes>>;
    fn send_message<B: Into<Bytes>>(
        &mut self,
        client_id: ClientId,
        channel_id: u8,
        message: B,
    ) -> impl Future<Output = ()>;
    fn client_ids(&self) -> Vec<ClientId>;

    fn update(&mut self, duration: std::time::Duration) -> impl Future<Output = ()> + '_;

    fn get_event(&mut self) -> Option<ServerEvent>;
}

pub trait HeadlessClient {
    fn new(id: ClientId, addr: impl ToSocketAddrs) -> impl Future<Output = Self>
    where
        Self: Send + Sized;
    fn receive_message<I: Into<u8>>(&mut self, channel: I) -> impl Future<Output = Option<Bytes>>;
    fn send_message<B: Into<Bytes>>(
        &mut self,
        channel_id: u8,
        message: B,
    ) -> impl Future<Output = ()>;
    fn update(&mut self, duration: std::time::Duration) -> impl Future<Output = ()> + '_;
    fn update_transport(&mut self, _: std::time::Duration) -> impl Future<Output = ()> + '_ {
        async {}
    }
    fn is_connected(&self) -> bool;

    fn send_packets(&mut self) -> impl Future<Output = ()> + '_ {
        async {}
    }
}
