use std::collections::HashMap;
use std::sync::atomic::AtomicU32;
use std::time::{Duration, Instant};

use rust_raknet::RaknetSocket;
#[cfg(feature = "quic")]
use s2n_quic::stream::BidirectionalStream;
use sctp_rs::ConnectedSocket;
use tokio::net::TcpStream;
use tokio::time::sleep;
use tracing::info;

use crate::args::{RunnerArgs, TransportType};
use crate::client::impls::ClientTransport;
use crate::client::raknet::{Reliable, Unreliable};
use crate::transports::ClientId;
use crate::types::Vec3;
use crate::types::{
    Entity, Message, NetworkedEntities, PlayerCommand, ServerEvent, ServerMessages, Vec2,
};

use self::impls::{ServerImpl, ServerTransport};

pub mod dccp;
pub mod impls;
#[cfg(feature = "quic")]
pub mod quic;
pub mod raknet;
pub mod sctp;
pub mod tcp;

pub async fn run_server(args: RunnerArgs) {
    let transport = args.transport;
    match transport {
        TransportType::Tcp => run::<TcpStream, _>(args).await,
        #[cfg(feature = "quic")]
        TransportType::Quic => run::<BidirectionalStream, _>(args).await,
        #[cfg(not(feature = "quic"))]
        TransportType::Quic => panic!("Quic not enabled"),
        TransportType::Sctp => run::<ConnectedSocket, _>(args).await,
        TransportType::RakNetReliable => run::<(RaknetSocket, Reliable), _>(args).await,
        TransportType::RakNetUnreliable => run::<(RaknetSocket, Unreliable), _>(args).await,
        TransportType::DCCP => run::<::dccp::DCCPSocket, _>(args).await,
    }
}

#[derive(Debug, Default)]
struct ServerLobbyHeadless {
    pub players: HashMap<ClientId, PlayerHeadless>,
    pub fireballs: HashMap<Entity, Fireball>,
    pub removed_fireballs: Vec<Entity>,
    pub removed_players: Vec<ClientId>,
}

impl ServerLobbyHeadless {
    fn new_fireball(&mut self, id: Entity, position: Vec3, direction: Vec3) {
        let expire_time = Instant::now() + Duration::from_secs(5);
        let fireball = Fireball {
            position,
            direction,
            expire_time,
        };
        self.fireballs.insert(id, fireball);
    }

    fn update_fireballs(&mut self, d: Duration) {
        let now = Instant::now();
        let removed_fireballs = self
            .fireballs
            .iter()
            .filter_map(|(entity, fireball)| {
                if fireball.expire_time < now {
                    Some(*entity)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        for entity in removed_fireballs {
            self.fireballs.remove(&entity);
            self.removed_fireballs.push(entity);
        }
        for fireball in self.fireballs.values_mut() {
            fireball.position += fireball.direction * d.as_secs_f32() * 10.0;
        }
    }

    fn entities_and_transforms(&self) -> Vec<(Entity, [f32; 3])> {
        let mut entities = Vec::new();
        for player in self.players.values() {
            entities.push((player.entity, player.position.into()));
        }
        for (entity, fireball) in &self.fireballs {
            entities.push((*entity, fireball.position.into()));
        }
        entities
    }
}

#[derive(Debug)]
struct PlayerHeadless {
    entity: Entity,
    position: Vec3,
}

#[derive(Debug)]
struct Fireball {
    position: Vec3,
    direction: Vec3,
    expire_time: Instant,
}

const PLAYER_MOVE_SPEED: f32 = 5.0;

static NEXT_ENTITY: AtomicU32 = AtomicU32::new(1);

fn next_entity() -> Entity {
    Entity::new(NEXT_ENTITY.fetch_add(1, std::sync::atomic::Ordering::Relaxed))
}

async fn run<C: ClientTransport, S>(args: RunnerArgs)
where
    S: ServerTransport,
    C: ClientTransport<Server = S> + 'static,
{
    let mut s = ServerImpl::<C>::new(args).await;
    let mut last_time = Instant::now();
    let mut lobby = ServerLobbyHeadless::default();
    loop {
        let time = last_time.elapsed();
        last_time = Instant::now();
        s.update(time).await;
        update_server_system(&mut s, &mut lobby, time).await;
        sleep(Duration::from_millis(1)).await;
    }
}

async fn update_server_system<S: ServerTransport, C: ClientTransport<Server = S> + 'static>(
    s: &mut ServerImpl<C>,
    lobby: &mut ServerLobbyHeadless,
    duration: Duration,
) {
    s.update(duration).await;

    for client_id in s.client_ids() {
        while let Some(message) = s.receive_message(client_id).await {
            // println!("Received message from {}: {:?}", client_id, message);
            lobby.update_fireballs(duration);
            match message {
                Message::Input(input) => {
                    if let Some(player_entity) = lobby.players.get_mut(&client_id) {
                        let x = (input.right as i8 - input.left as i8) as f32;
                        let y = (input.down as i8 - input.up as i8) as f32;
                        let direction = Vec2::new(x, y).normalize_or_zero();
                        player_entity.position.x +=
                            direction.x * PLAYER_MOVE_SPEED * duration.as_secs_f32();
                        player_entity.position.z +=
                            direction.y * PLAYER_MOVE_SPEED * duration.as_secs_f32();
                    }
                }
                Message::Command(command) => {
                    match command {
                        PlayerCommand::BasicAttack { mut cast_at } => {
                            // println!(
                            //     "Received basic attack from client {:?}: {:?}",
                            //     client_id, cast_at
                            // );

                            if let Some(player_headless) = lobby.players.get(&client_id) {
                                let player_transform = player_headless.position;
                                cast_at[1] = player_transform[1];

                                let direction = (cast_at - player_transform).normalize_or_zero();
                                let mut translation = player_transform + (direction * 0.7);
                                translation[1] = 1.0;

                                let fireball_entity = next_entity();
                                lobby.new_fireball(fireball_entity, translation, direction);
                                let message = ServerMessages::SpawnProjectile {
                                    entity: fireball_entity,
                                    translation: translation.into(),
                                };
                                s.broadcast(message.into()).await;
                            }
                        }
                    }
                }
                _ => unreachable!(),
            }
        }
    }
    while let Some(event) = s.get_event() {
        match event {
            ServerEvent::ClientConnected(id) => {
                info!("Player {} connected.", id);
                for (other_id, player) in lobby.players.iter() {
                    let translation: [f32; 3] = player.position.into();
                    s.send_message(
                        id,
                        ServerMessages::PlayerCreate {
                            id: *other_id,
                            entity: player.entity,
                            translation,
                        }
                        .into(),
                    )
                    .await;
                }

                let position = Vec3::new(
                    (fastrand::f32() - 0.5) * 40.,
                    0.51,
                    (fastrand::f32() - 0.5) * 40.,
                );

                let player_entity = next_entity();

                lobby.players.insert(
                    id,
                    PlayerHeadless {
                        entity: player_entity,
                        position,
                    },
                );

                let translation: [f32; 3] = position.into();
                let message = ServerMessages::PlayerCreate {
                    id,
                    entity: player_entity,
                    translation,
                };

                // thread::sleep(Duration::from_millis(50));
                s.broadcast(message.into()).await;
            }
            ServerEvent::ClientDisconnected(id) => {
                if lobby.players.remove(&id).is_some() {
                    lobby.removed_players.push(id);
                }

                let message = ServerMessages::PlayerRemove { id };
                s.broadcast(message.into()).await;
            }
        }
    }

    let mut networked_entities = NetworkedEntities::default();
    for (entity, transform) in lobby.entities_and_transforms() {
        networked_entities.entities.push(entity);
        networked_entities.translations.push(transform);
    }

    while let Some(e) = lobby.removed_fireballs.pop() {
        let message = ServerMessages::DespawnProjectile { entity: e };
        s.broadcast(message.into()).await;
    }
    while let Some(p) = lobby.removed_players.pop() {
        let message = ServerMessages::PlayerRemove { id: p };
        s.broadcast(message.into()).await;
    }

    s.broadcast(networked_entities.into()).await;
}
