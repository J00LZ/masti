{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
  inputs.fup.url = "github:gytis-ivaskevicius/flake-utils-plus/v1.4.0";

  outputs = inputs@{ self, nixpkgs, fup }:
    let pkgs = self.pkgs.x86_64-linux.nixpkgs;
    in fup.lib.mkFlake {
      inherit self inputs;

      channelsConfig = { allowUnfree = true; };
      outputsBuilder = channels: {
        devShells.default = with channels.nixpkgs;
          mkShell rec {
            packages = [ ];
            nativeBuildInputs = [ 
              pkg-config
              cmake
              clang
              go
            ];
            buildInputs = [
              openssl_3
              llvmPackages.clangUseLLVM
              libunwind
              udev
              alsa-lib
              vulkan-loader
              xorg.libX11
              xorg.libXcursor
              xorg.libXi
              xorg.libXrandr # To use the x11 feature
              libxkbcommon
              wayland # To use the wayland feature
            ];

            S2N_LIBCRYPTO = "openssl-3.0";
            LD_LIBRARY_PATH = lib.makeLibraryPath buildInputs;
          };
      };
    };
}

