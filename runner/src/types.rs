use serde::{Deserialize, Serialize};

use crate::transports::ClientId;

/// Change transports here

// pub type SelectedClient = TcpClient;
// pub type SelectedServer = TcpServer;

// pub const PRIVATE_KEY: &[u8; 32] = b"an example very very secret key."; // 32-bytes

#[derive(Debug, Serialize, Deserialize)]
pub struct MessageWrapper(Message, u64);

impl MessageWrapper {
    pub fn new(message: Message, sequence: u64) -> Self {
        Self(message, sequence)
    }

    pub fn destructure(self) -> (Message, u64) {
        (self.0, self.1)
    }
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum Message {
    Input(PlayerInput),
    Command(PlayerCommand),
    ServerMessage(ServerMessages),
    Entities(NetworkedEntities),
    Ack(u64),
}

impl From<PlayerInput> for Message {
    fn from(value: PlayerInput) -> Self {
        Message::Input(value)
    }
}

impl From<PlayerCommand> for Message {
    fn from(value: PlayerCommand) -> Self {
        Message::Command(value)
    }
}

impl From<ServerMessages> for Message {
    fn from(value: ServerMessages) -> Self {
        Message::ServerMessage(value)
    }
}

impl From<NetworkedEntities> for Message {
    fn from(value: NetworkedEntities) -> Self {
        Message::Entities(value)
    }
}

#[derive(Debug, Copy, Clone)]
pub enum ServerEvent {
    ClientConnected(ClientId),
    ClientDisconnected(ClientId),
}

#[derive(Debug)]
pub struct Player {
    pub id: ClientId,
}

#[derive(Debug, Default, Clone, Copy, Serialize, Deserialize)]
pub struct PlayerInput {
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub enum PlayerCommand {
    BasicAttack { cast_at: [f32; 3] },
}

#[repr(u8)]
pub enum ClientChannel {
    Input = 1,
    Command = 2,
}

#[repr(u8)]
pub enum ServerChannel {
    ServerMessages = 10,
    NetworkedEntities = 11,
}

// pub const ACK_CHANNEL: u8 = 42;

#[derive(Debug, Deserialize, Serialize, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Entity(pub u32);

impl Entity {
    pub fn new(id: u32) -> Self {
        Self(id)
    }
}

#[derive(Debug, Default)]
pub struct Velocity(pub [f32; 3]);

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum ServerMessages {
    PlayerCreate {
        entity: Entity,
        id: ClientId,
        translation: [f32; 3],
    },
    PlayerRemove {
        id: ClientId,
    },
    SpawnProjectile {
        entity: Entity,
        translation: [f32; 3],
    },
    DespawnProjectile {
        entity: Entity,
    },
    Helo {
        id: ClientId,
    },
}

#[derive(Debug, Serialize, Deserialize, Default, Clone)]
pub struct NetworkedEntities {
    pub entities: Vec<Entity>,
    pub translations: Vec<[f32; 3]>,
}

impl From<ClientChannel> for u8 {
    fn from(channel_id: ClientChannel) -> Self {
        channel_id as u8
    }
}

impl From<ServerChannel> for u8 {
    fn from(channel_id: ServerChannel) -> Self {
        channel_id as u8
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

mod vec3 {
    use super::Vec3;

    impl Vec3 {
        pub fn new(x: f32, y: f32, z: f32) -> Self {
            Self { x, y, z }
        }

        pub fn normalize_or_zero(self) -> Self {
            let len = self.length();
            if len == 0.0 {
                Self::new(0.0, 0.0, 0.0)
            } else {
                self / len
            }
        }

        pub fn length(self) -> f32 {
            (self.x * self.x + self.y * self.y + self.z * self.z).sqrt()
        }
    }

    impl From<[f32; 3]> for Vec3 {
        fn from(value: [f32; 3]) -> Self {
            Self {
                x: value[0],
                y: value[1],
                z: value[2],
            }
        }
    }

    impl From<Vec3> for [f32; 3] {
        fn from(value: Vec3) -> Self {
            [value.x, value.y, value.z]
        }
    }

    impl std::ops::Add for Vec3 {
        type Output = Self;

        fn add(self, rhs: Self) -> Self::Output {
            Self {
                x: self.x + rhs.x,
                y: self.y + rhs.y,
                z: self.z + rhs.z,
            }
        }
    }

    impl std::ops::Sub for Vec3 {
        type Output = Self;

        fn sub(self, rhs: Self) -> Self::Output {
            Self {
                x: self.x - rhs.x,
                y: self.y - rhs.y,
                z: self.z - rhs.z,
            }
        }
    }

    impl std::ops::Mul<f32> for Vec3 {
        type Output = Self;

        fn mul(self, rhs: f32) -> Self::Output {
            Self {
                x: self.x * rhs,
                y: self.y * rhs,
                z: self.z * rhs,
            }
        }
    }

    impl std::ops::Div<f32> for Vec3 {
        type Output = Self;

        fn div(self, rhs: f32) -> Self::Output {
            Self {
                x: self.x / rhs,
                y: self.y / rhs,
                z: self.z / rhs,
            }
        }
    }

    impl std::ops::AddAssign for Vec3 {
        fn add_assign(&mut self, rhs: Self) {
            *self = *self + rhs;
        }
    }

    impl std::ops::Index<usize> for Vec3 {
        type Output = f32;

        fn index(&self, index: usize) -> &Self::Output {
            match index {
                0 => &self.x,
                1 => &self.y,
                2 => &self.z,
                _ => panic!("Index out of bounds"),
            }
        }
    }

    impl std::ops::IndexMut<usize> for Vec3 {
        fn index_mut(&mut self, index: usize) -> &mut Self::Output {
            match index {
                0 => &mut self.x,
                1 => &mut self.y,
                2 => &mut self.z,
                _ => panic!("Index out of bounds"),
            }
        }
    }

    impl std::ops::Sub<[f32; 3]> for Vec3 {
        type Output = Self;

        fn sub(self, rhs: [f32; 3]) -> Self::Output {
            Self {
                x: self.x - rhs[0],
                y: self.y - rhs[1],
                z: self.z - rhs[2],
            }
        }
    }

    impl std::ops::Sub<Vec3> for [f32; 3] {
        type Output = Vec3;

        fn sub(self, rhs: Vec3) -> Self::Output {
            Vec3 {
                x: self[0] - rhs.x,
                y: self[1] - rhs.y,
                z: self[2] - rhs.z,
            }
        }
    }
}

#[derive(Debug, Serialize, Deserialize, Clone, Copy)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

mod vec2 {
    use super::Vec2;

    impl Vec2 {
        pub fn new(x: f32, y: f32) -> Self {
            Self { x, y }
        }

        pub fn normalize_or_zero(self) -> Self {
            let len = self.length();
            if len == 0.0 {
                Self::new(0.0, 0.0)
            } else {
                self / len
            }
        }

        pub fn length(self) -> f32 {
            (self.x * self.x + self.y * self.y).sqrt()
        }
    }

    impl From<[f32; 2]> for Vec2 {
        fn from(value: [f32; 2]) -> Self {
            Self {
                x: value[0],
                y: value[1],
            }
        }
    }

    impl From<Vec2> for [f32; 2] {
        fn from(value: Vec2) -> Self {
            [value.x, value.y]
        }
    }

    impl std::ops::Add for Vec2 {
        type Output = Self;

        fn add(self, rhs: Self) -> Self::Output {
            Self {
                x: self.x + rhs.x,
                y: self.y + rhs.y,
            }
        }
    }

    impl std::ops::Sub for Vec2 {
        type Output = Self;

        fn sub(self, rhs: Self) -> Self::Output {
            Self {
                x: self.x - rhs.x,
                y: self.y - rhs.y,
            }
        }
    }

    impl std::ops::Mul<f32> for Vec2 {
        type Output = Self;

        fn mul(self, rhs: f32) -> Self::Output {
            Self {
                x: self.x * rhs,
                y: self.y * rhs,
            }
        }
    }

    impl std::ops::Div<f32> for Vec2 {
        type Output = Self;

        fn div(self, rhs: f32) -> Self::Output {
            Self {
                x: self.x / rhs,
                y: self.y / rhs,
            }
        }
    }

    impl std::ops::AddAssign for Vec2 {
        fn add_assign(&mut self, rhs: Self) {
            *self = *self + rhs;
        }
    }

    impl std::ops::Index<usize> for Vec2 {
        type Output = f32;

        fn index(&self, index: usize) -> &Self::Output {
            match index {
                0 => &self.x,
                1 => &self.y,
                _ => panic!("Index out of bounds"),
            }
        }
    }

    impl std::ops::IndexMut<usize> for Vec2 {
        fn index_mut(&mut self, index: usize) -> &mut Self::Output {
            match index {
                0 => &mut self.x,
                1 => &mut self.y,
                _ => panic!("Index out of bounds"),
            }
        }
    }
}
