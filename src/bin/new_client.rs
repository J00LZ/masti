use std::fs::{File, OpenOptions};
use std::process::exit;
use std::time::{Duration, Instant};

use demo_bevy::transports::quic::client::ActualQuicClient;
use s2n_quic::stream::BidirectionalStream;
use serde::{Deserialize, Serialize};
use tokio::net::TcpStream;
use tokio::time::sleep;

use demo_bevy::transports::transport::ClientImpl;
use demo_bevy::transports::transport::ClientTransport;
use demo_bevy::{Message, PlayerCommand, PlayerInput};

#[derive(Serialize, Deserialize)]
enum Foo {
    PlayerInput(PlayerInput),
    PlayerCommand(PlayerCommand),
}

impl From<Foo> for Message {
    fn from(value: Foo) -> Self {
        match value {
            Foo::PlayerInput(p) => Message::Input(p),
            Foo::PlayerCommand(p) => Message::Command(p),
        }
    }
}

#[derive(Serialize, Deserialize)]
struct Bar {
    pub foo: Foo,
    pub time: Duration,
}

#[tokio::main]
async fn main() {
    let log = "./logs/test.json";
    // Read log file
    let file = File::open(log).unwrap();
    let mut inputs: Vec<Bar> = serde_json::from_reader(file).unwrap();
    inputs.reverse(); // Newest at the back so we can pop
                      // remove the last input if it'll take too long to get there
    if inputs[0].time > Duration::from_secs(100) {
        inputs.remove(0);
    }
    inputs.reverse();

    // Initialize tcp client
    // let mut client = SelectedClient::init(client_id); // tcp client gets id from server
    // let mut c = HC::<TcpClient>::new(ClientId(client_id), "127.0.0.1:5000").await;
    let client = ClientImpl::<TcpStream>::start("127.0.0.1:5000").await;
    println!("Connected!");

    client.stats.set_should_send(inputs.len());

    // Fake game loop
    run_inputs(&client, inputs).await;

    let stats = client.stats;
    println!("Stats: {:?}", stats);
    let mut f = OpenOptions::new()
        .append(true)
        .create(true)
        .open("./out.csv")
        .unwrap();
    stats.write_all(&mut f);

    exit(0)
}

async fn run_inputs<C: 'static + ClientTransport>(client: &ClientImpl<C>, inputs: Vec<Bar>) {
    let frame_duration = Duration::from_secs_f32(1.0 / 60.0);
    let mut inputs = inputs.into_iter().peekable();
    let mut last_time = Instant::now();
    let start_time = Instant::now();

    loop {
        let elapsed = last_time.elapsed();
        if elapsed < frame_duration {
            // tokio::time::sleep(frame_duration - elapsed).await;
        } else {
            eprintln!("Could not keep up")
        }
        last_time = Instant::now();
        client.update(elapsed);
        // client connected
        if client.is_connected() {
            while let Some(m) = client.receive_message() {
                println!("Received message: {m:?}");
            }
            while let Some(i) = inputs.peek() {
                if start_time.elapsed() >= i.time {
                    let i = inputs.next().unwrap();
                    client.send_message(i.foo.into());
                } else {
                    break;
                }
            }
        }

        if inputs.peek().is_none() {
            sleep(Duration::from_millis(250)).await;
            break;
        }

        sleep(Duration::from_millis(1)).await;
    }
}
