use std::{
    collections::HashMap,
    f32::consts::PI,
    net::ToSocketAddrs,
    time::{Duration, Instant},
};

use bevy::{diagnostic::LogDiagnosticsPlugin, prelude::*};
use bevy_egui::{EguiContexts, EguiPlugin};
use bevy_renet::renet::ServerEvent;
use demo_bevy::transports::{quic::server::QuicServer, renet::server::RenetServer2, tcp::server::TcpServer, Server as ServerTrait};
use demo_bevy::{
    setup_level, spawn_fireball,
    transports::{self, headless_traits::HeadlessServer, ClientId},
    ClientChannel, NetworkedEntities, Player, PlayerCommand, PlayerInput, Projectile,
    SelectedServer, ServerChannel, ServerMessages, Velocity,
};
use renet_visualizer::RenetServerVisualizer;

#[derive(Debug, Default, Resource)]
pub struct ServerLobby {
    pub players: HashMap<ClientId, Entity>,
}

#[derive(Debug, Default)]
struct ServerLobbyHeadless {
    pub players: HashMap<ClientId, PlayerHeadless>,
    pub fireballs: HashMap<Entity, Fireball>,
    pub removed_fireballs: Vec<Entity>,
    pub removed_players: Vec<ClientId>,
}
impl ServerLobbyHeadless {
    fn new_fireball(&mut self, id: Entity, position: Vec3, direction: Vec3) {
        let expire_time = Instant::now() + Duration::from_secs(5);
        let fireball = Fireball {
            position,
            direction,
            expire_time,
        };
        self.fireballs.insert(id, fireball);
    }

    fn update_fireballs(&mut self, d: Duration) {
        let now = Instant::now();
        let removed_fireballs = self
            .fireballs
            .iter()
            .filter_map(|(entity, fireball)| {
                if fireball.expire_time < now {
                    Some(*entity)
                } else {
                    None
                }
            })
            .collect::<Vec<_>>();
        for entity in removed_fireballs {
            self.fireballs.remove(&entity);
            self.removed_fireballs.push(entity);
        }
        for fireball in self.fireballs.values_mut() {
            fireball.position += fireball.direction * d.as_secs_f32() * 10.0;
        }
    }

    fn entities_and_transforms(&self) -> Vec<(Entity, [f32; 3])> {
        let mut entities = Vec::new();
        for player in self.players.values() {
            entities.push((player.entity, player.position.into()));
        }
        for (entity, fireball) in &self.fireballs {
            entities.push((*entity, fireball.position.into()));
        }
        entities
    }
}

#[derive(Debug)]
struct PlayerHeadless {
    entity: Entity,
    position: Vec3,
}

#[derive(Debug)]
struct Fireball {
    position: Vec3,
    direction: Vec3,
    expire_time: Instant,
}

const PLAYER_MOVE_SPEED: f32 = 5.0;

#[derive(Debug, Component)]
struct Bot {
    auto_cast: Timer,
}

#[derive(Debug, Resource)]
struct BotId(u64);

async fn headless_server(addr: impl ToSocketAddrs) -> ! {
    let mut s = Server::<TcpServer>::new(addr).await;
    let mut last_time = std::time::Instant::now();
    loop {
        let time = last_time.elapsed();
        last_time = std::time::Instant::now();
        s.update(time).await;
        s.update_server_system().await;
        tokio::time::sleep_until((last_time + Duration::from_millis(16)).into()).await;
    }
}

struct Server<S> {
    server: S,
    lobby: ServerLobbyHeadless,
    next_entity: u32,
}

impl<S: HeadlessServer + Send> Server<S> {
    pub async fn new(addr: impl ToSocketAddrs) -> Self {
        let server = S::new(addr).await;
        Self {
            server,
            lobby: ServerLobbyHeadless::default(),
            next_entity: 1,
        }
    }

    pub async fn update(&mut self, duration: Duration) {
        self.server.update(duration).await;
        for client_id in self.server.client_ids() {
            while let Some(message) = self
                .server
                .receive_message(client_id, ClientChannel::Input)
                .await
            {
                let input: PlayerInput = bincode::deserialize(&message).unwrap();
                println!("received input {:?}", input);
                if let Some(player_entity) = self.lobby.players.get_mut(&client_id) {
                    let x = (input.right as i8 - input.left as i8) as f32;
                    let y = (input.down as i8 - input.up as i8) as f32;
                    let direction = Vec2::new(x, y).normalize_or_zero();
                    player_entity.position.x +=
                        direction.x * PLAYER_MOVE_SPEED * duration.as_secs_f32();
                    player_entity.position.z +=
                        direction.y * PLAYER_MOVE_SPEED * duration.as_secs_f32();
                }
            }
        }
        self.lobby.update_fireballs(duration);
    }

    fn next_entity(&mut self) -> Entity {
        let entity = self.next_entity;
        self.next_entity += 1;
        Entity::from_raw(entity)
    }

    async fn update_server_system(&mut self) {
        while let Some(event) = self.server.get_event() {
            match event {
                ServerEvent::ClientConnected { client_id: renetid } => {
                    let client_id: transports::ClientId = renetid.into();
                    info!("Player {} connected.", client_id.0);
                    // visualizer.add_client(*renetid);

                    // TODO: Add visualizer?

                    // Initialize other players for this new client
                    for (id, player) in self.lobby.players.iter() {
                        let translation: [f32; 3] = player.position.into();
                        let message = bincode::serialize(&ServerMessages::PlayerCreate {
                            id: *id,
                            entity: player.entity,
                            translation,
                        })
                        .unwrap();
                        self.server
                            .send_message(client_id, ServerChannel::ServerMessages as u8, message)
                            .await;
                    }

                    // Spawn new player
                    let position = Vec3::new(
                        (fastrand::f32() - 0.5) * 40.,
                        0.51,
                        (fastrand::f32() - 0.5) * 40.,
                    );

                    let player_entity = self.next_entity();

                    self.lobby.players.insert(
                        client_id,
                        PlayerHeadless {
                            entity: player_entity,
                            position,
                        },
                    );

                    let translation: [f32; 3] = position.into();
                    let message = bincode::serialize(&ServerMessages::PlayerCreate {
                        id: client_id,
                        entity: player_entity,
                        translation,
                    })
                    .unwrap();
                    // thread::sleep(Duration::from_millis(50));
                    self.server
                        .broadcast_message(ServerChannel::ServerMessages as u8, message)
                        .await;
                }
                ServerEvent::ClientDisconnected { client_id, reason } => {
                    println!("Player {} disconnected: {}", client_id, reason);
                    // visualizer.remove_client(*client_id);
                    let cid: ClientId = ClientId(client_id.raw());
                    if let Some(_) = self.lobby.players.remove(&cid) {
                        self.lobby.removed_players.push(cid);
                    }

                    let message = bincode::serialize(&ServerMessages::PlayerRemove {
                        id: client_id.into(),
                    })
                    .unwrap();
                    self.server
                        .broadcast_message(ServerChannel::ServerMessages as u8, message)
                        .await;
                }
            }
        }

        for client_id in self.server.client_ids() {
            while let Some(message) = self
                .server
                .receive_message(client_id, ClientChannel::Command)
                .await
            {
                let command: PlayerCommand = bincode::deserialize(&message).unwrap();
                match command {
                    PlayerCommand::BasicAttack { mut cast_at } => {
                        println!(
                            "Received basic attack from client {:?}: {:?}",
                            client_id, cast_at
                        );

                        if let Some(player_headless) = self.lobby.players.get(&client_id) {
                            let player_transform = player_headless.position;
                            cast_at[1] = player_transform[1];

                            let direction = (cast_at - player_transform).normalize_or_zero();
                            let mut translation = player_transform + (direction * 0.7);
                            translation[1] = 1.0;

                            let fireball_entity = self.next_entity();
                            self.lobby
                                .new_fireball(fireball_entity, translation, direction);
                            let message = ServerMessages::SpawnProjectile {
                                entity: fireball_entity,
                                translation: translation.into(),
                            };
                            let message = bincode::serialize(&message).unwrap();
                            self.server
                                .broadcast_message(ServerChannel::ServerMessages as u8, message)
                                .await;
                        }
                    }
                }
            }
        }

        let mut networked_entities = NetworkedEntities::default();
        for (entity, transform) in self.lobby.entities_and_transforms() {
            networked_entities.entities.push(entity);
            networked_entities.translations.push(transform);
        }

        while let Some(e) = self.lobby.removed_fireballs.pop() {
            let message =
                bincode::serialize(&ServerMessages::DespawnProjectile { entity: e }).unwrap();
            self.server
                .broadcast_message(ServerChannel::ServerMessages as u8, message)
                .await;
        }
        while let Some(p) = self.lobby.removed_players.pop() {
            let message = bincode::serialize(&ServerMessages::PlayerRemove { id: p }).unwrap();
            self.server
                .broadcast_message(ServerChannel::ServerMessages as u8, message)
                .await;
        }

        let sync_message = bincode::serialize(&networked_entities).unwrap();
        self.server
            .broadcast_message(ServerChannel::NetworkedEntities as u8, sync_message)
            .await;
    }
}

#[tokio::main]
async fn main() {
    headless_server("127.0.0.1:5000").await;

    let mut app = App::new();
    app.add_plugins(DefaultPlugins);

    // app.add_plugins(FrameTimeDiagnosticsPlugin);
    app.add_plugins(LogDiagnosticsPlugin::default());
    app.add_plugins(EguiPlugin);

    app.insert_resource(ServerLobby::default());
    app.insert_resource(BotId(0));

    app.insert_resource(RenetServerVisualizer::<200>::default());

    SelectedServer::init_bevy(&mut app);

    app.add_systems(
        Update,
        (
            server_update_system::<SelectedServer>,
            server_network_sync::<SelectedServer>,
            move_players_system,
            update_projectiles_system,
            update_visulizer_system,
            spawn_bot::<SelectedServer>,
            bot_autocast::<SelectedServer>,
        ),
    );

    app.add_systems(FixedUpdate, apply_velocity_system);

    app.add_systems(PostUpdate, projectile_on_removal_system::<SelectedServer>);

    app.add_systems(Startup, (setup_level, setup_simple_camera));

    app.run();
}

#[allow(clippy::too_many_arguments)]
fn server_update_system<C: ServerTrait>(
    mut server_events: EventReader<ServerEvent>,
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut lobby: ResMut<ServerLobby>,
    mut server: ResMut<C>,
    mut visualizer: ResMut<RenetServerVisualizer<200>>,
    players: Query<(Entity, &Player, &Transform)>,
) {
    for event in server_events.read() {
        match event {
            ServerEvent::ClientConnected { client_id: renetid } => {
                let client_id: transports::ClientId = (*renetid).into();
                info!("Player {} connected.", client_id.0);
                visualizer.add_client(*renetid);

                // Initialize other players for this new client
                for (entity, player, transform) in players.iter() {
                    let translation: [f32; 3] = transform.translation.into();
                    let message = bincode::serialize(&ServerMessages::PlayerCreate {
                        id: player.id,
                        entity,
                        translation,
                    })
                    .unwrap();
                    server.send_message(client_id, ServerChannel::ServerMessages, message);
                }

                // Spawn new player
                let transform = Transform::from_xyz(
                    (fastrand::f32() - 0.5) * 40.,
                    0.51,
                    (fastrand::f32() - 0.5) * 40.,
                );
                let player_entity = commands
                    .spawn(PbrBundle {
                        mesh: meshes.add(Mesh::from(Capsule3d::default())),
                        material: materials.add(Color::rgb(0.8, 0.7, 0.6)),
                        transform,
                        ..Default::default()
                    })
                    .insert(PlayerInput::default())
                    .insert(Velocity::default())
                    .insert(Player { id: client_id })
                    .id();

                lobby.players.insert(client_id, player_entity);

                let translation: [f32; 3] = transform.translation.into();
                let message = bincode::serialize(&ServerMessages::PlayerCreate {
                    id: client_id,
                    entity: player_entity,
                    translation,
                })
                .unwrap();
                // thread::sleep(Duration::from_millis(50));
                server.broadcast_message(ServerChannel::ServerMessages, message);
            }
            ServerEvent::ClientDisconnected { client_id, reason } => {
                println!("Player {} disconnected: {}", client_id, reason);
                visualizer.remove_client(*client_id);
                let cid: ClientId = ClientId(client_id.raw());
                if let Some(player_entity) = lobby.players.remove(&cid) {
                    commands.entity(player_entity).despawn();
                }

                let message = bincode::serialize(&ServerMessages::PlayerRemove {
                    id: (*client_id).into(),
                })
                .unwrap();
                server.broadcast_message(ServerChannel::ServerMessages, message);
            }
        }
    }

    for client_id in server.clients_id() {
        while let Some(message) = server.receive_message(client_id, ClientChannel::Command) {
            let command: PlayerCommand = bincode::deserialize(&message).unwrap();
            match command {
                PlayerCommand::BasicAttack { mut cast_at } => {
                    println!(
                        "Received basic attack from client {:?}: {:?}",
                        client_id, cast_at
                    );

                    if let Some(player_entity) = lobby.players.get(&client_id) {
                        if let Ok((_, _, player_transform)) = players.get(*player_entity) {
                            cast_at[1] = player_transform.translation[1];

                            let direction =
                                (cast_at - player_transform.translation).normalize_or_zero();
                            let mut translation = player_transform.translation + (direction * 0.7);
                            translation[1] = 1.0;

                            let fireball_entity = spawn_fireball(
                                &mut commands,
                                &mut meshes,
                                &mut materials,
                                translation,
                                direction,
                            );
                            let message = ServerMessages::SpawnProjectile {
                                entity: fireball_entity,
                                translation: translation.into(),
                            };
                            let message = bincode::serialize(&message).unwrap();
                            server.broadcast_message(ServerChannel::ServerMessages, message);
                        }
                    }
                }
            }
        }
        while let Some(message) = server.receive_message(client_id, ClientChannel::Input) {
            let input: PlayerInput = bincode::deserialize(&message).unwrap();
            if let Some(player_entity) = lobby.players.get(&client_id) {
                commands.entity(*player_entity).insert(input);
            }
        }
    }
}

fn update_projectiles_system(
    mut commands: Commands,
    mut projectiles: Query<(Entity, &mut Projectile)>,
    time: Res<Time>,
) {
    for (entity, mut projectile) in projectiles.iter_mut() {
        projectile.duration.tick(time.delta());
        if projectile.duration.finished() {
            commands.entity(entity).despawn();
        }
    }
}

fn update_visulizer_system(
    mut egui_contexts: EguiContexts,
    mut visualizer: ResMut<RenetServerVisualizer<200>>,
    server: Res<SelectedServer>,
) {
    // visualizer.update(&server);
    for c in server.clients_id() {
        if let Some(_ni) = <SelectedServer as ServerTrait>::network_info(&server, c.into()) {}
    }
    visualizer.show_window(egui_contexts.ctx_mut());
}

#[allow(clippy::type_complexity)]
fn server_network_sync<S: ServerTrait>(
    mut server: ResMut<S>,
    query: Query<(Entity, &Transform), Or<(With<Player>, With<Projectile>)>>,
) {
    let mut networked_entities = NetworkedEntities::default();
    for (entity, transform) in query.iter() {
        networked_entities.entities.push(entity);
        networked_entities
            .translations
            .push(transform.translation.into());
    }

    let sync_message = bincode::serialize(&networked_entities).unwrap();
    server.broadcast_message(ServerChannel::NetworkedEntities, sync_message);
}

fn move_players_system(mut query: Query<(&mut Velocity, &PlayerInput)>) {
    for (mut velocity, input) in query.iter_mut() {
        let x = (input.right as i8 - input.left as i8) as f32;
        let y = (input.down as i8 - input.up as i8) as f32;
        let direction = Vec2::new(x, y).normalize_or_zero();
        velocity.0.x = direction.x * PLAYER_MOVE_SPEED;
        velocity.0.z = direction.y * PLAYER_MOVE_SPEED;
    }
}

fn apply_velocity_system(mut query: Query<(&Velocity, &mut Transform)>, time: Res<Time>) {
    for (velocity, mut transform) in query.iter_mut() {
        transform.translation += velocity.0 * time.delta_seconds();
    }
}

pub fn setup_simple_camera(mut commands: Commands) {
    // camera
    commands.spawn(Camera3dBundle {
        transform: Transform::from_xyz(-20.5, 30.0, 20.5).looking_at(Vec3::ZERO, Vec3::Y),
        ..Default::default()
    });
}

fn projectile_on_removal_system<S: ServerTrait>(
    mut server: ResMut<S>,
    mut removed_projectiles: RemovedComponents<Projectile>,
) {
    for entity in removed_projectiles.read() {
        let message = ServerMessages::DespawnProjectile { entity };
        let message = bincode::serialize(&message).unwrap();

        server.broadcast_message(ServerChannel::ServerMessages, message);
    }
}

fn spawn_bot<S: ServerTrait>(
    keyboard_input: Res<ButtonInput<KeyCode>>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut lobby: ResMut<ServerLobby>,
    mut server: ResMut<S>,
    mut bot_id: ResMut<BotId>,
    mut commands: Commands,
) {
    if keyboard_input.just_pressed(KeyCode::Space) {
        let client_id = ClientId(bot_id.0);
        bot_id.0 += 1;
        // Spawn new player
        let transform = Transform::from_xyz(
            (fastrand::f32() - 0.5) * 40.,
            0.51,
            (fastrand::f32() - 0.5) * 40.,
        );
        let player_entity = commands
            .spawn(PbrBundle {
                mesh: meshes.add(Mesh::from(Capsule3d::default())),
                material: materials.add(Color::rgb(0.8, 0.7, 0.6)),
                transform,
                ..Default::default()
            })
            .insert(Player { id: client_id })
            .insert(Bot {
                auto_cast: Timer::from_seconds(3.0, TimerMode::Repeating),
            })
            .id();

        lobby.players.insert(client_id, player_entity);

        let translation: [f32; 3] = transform.translation.into();
        let message = bincode::serialize(&ServerMessages::PlayerCreate {
            id: client_id,
            entity: player_entity,
            translation,
        })
        .unwrap();
        server.broadcast_message(ServerChannel::ServerMessages, message);
    }
}

fn bot_autocast<S: ServerTrait>(
    time: Res<Time>,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    mut server: ResMut<S>,
    mut bots: Query<(&Transform, &mut Bot), With<Player>>,
    mut commands: Commands,
) {
    for (transform, mut bot) in &mut bots {
        bot.auto_cast.tick(time.delta());
        if !bot.auto_cast.just_finished() {
            continue;
        }

        for i in 0..8 {
            let direction = Vec2::from_angle(PI / 4. * i as f32);
            let direction = Vec3::new(direction.x, 0., direction.y).normalize();
            let translation: Vec3 = transform.translation + direction;

            let fireball_entity = spawn_fireball(
                &mut commands,
                &mut meshes,
                &mut materials,
                translation,
                direction,
            );
            let message = ServerMessages::SpawnProjectile {
                entity: fireball_entity,
                translation: translation.into(),
            };
            let message = bincode::serialize(&message).unwrap();
            server.broadcast_message(ServerChannel::ServerMessages, message);
        }
    }
}
