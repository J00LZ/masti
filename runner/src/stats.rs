use std::{
    io::Write,
    sync::{
        atomic::{AtomicU64, AtomicUsize},
        Arc, RwLock,
    },
};

#[derive(Debug, Clone, Default)]
pub struct Stats(Arc<InnerStats>);

#[derive(Debug, Default)]
pub struct InnerStats {
    proto: &'static str,
    communication_type: RwLock<String>,
    log_name: RwLock<String>,
    should_sent: AtomicUsize,

    bytes_sent: AtomicUsize,
    bytes_received: AtomicUsize,
    /// In Nanoseconds
    rtt_sum: AtomicU64,
    packets_sent: AtomicUsize,
    packets_received: AtomicUsize,
    rtt_count: AtomicUsize,
    rtts: lockfree::queue::Queue<u64>,
}

impl Stats {
    pub fn new(proto: &'static str) -> Self {
        Stats(Arc::new(InnerStats {
            proto,
            ..Default::default()
        }))
    }

    pub fn set_should_send(&self, n: usize) {
        self.0
            .should_sent
            .store(n, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn sent_packets(&self, packet_count: usize, byte_count: usize) {
        self.0
            .packets_sent
            .fetch_add(packet_count, std::sync::atomic::Ordering::SeqCst);
        self.0
            .bytes_sent
            .fetch_add(byte_count, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn bytes_received(&self, byte_count: usize) {
        self.0
            .bytes_received
            .fetch_add(byte_count, std::sync::atomic::Ordering::SeqCst);
    }

    /// rtt in nanoseconds
    pub fn rtt(&self, rtt: u64) {
        self.0.rtts.push(rtt);
        self.0
            .rtt_sum
            .fetch_add(rtt, std::sync::atomic::Ordering::Relaxed);
        self.0
            .rtt_count
            .fetch_add(1, std::sync::atomic::Ordering::Relaxed);
    }

    pub fn get_rtt(&self) -> f64 {
        self.0.rtt_sum.load(std::sync::atomic::Ordering::SeqCst) as f64
            / self.0.rtt_count.load(std::sync::atomic::Ordering::SeqCst) as f64
    }

    pub fn received_packet(&self) {
        self.0
            .packets_received
            .fetch_add(1, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn set_communication_type(&self, connection_type: String) {
        *self.0.communication_type.write().unwrap() = connection_type;
    }

    pub fn set_log_name(&self, log_name: String) {
        *self.0.log_name.write().unwrap() = log_name;
    }

    pub fn write_all(&self, w: &mut impl Write) {
        let len = self.0.rtt_count.load(std::sync::atomic::Ordering::SeqCst);

        let jitter = self
            .0
            .rtts
            .pop_iter()
            .map(|d| (d as f64 - self.get_rtt()).abs())
            .sum::<f64>()
            / len as f64;
        writeln!(
            w,
            "{},{},{},{},{},{},{},{},{}",
            self.0.proto,
            self.0.communication_type.read().unwrap(),
            self.0.bytes_sent.load(std::sync::atomic::Ordering::SeqCst),
            self.0
                .bytes_received
                .load(std::sync::atomic::Ordering::SeqCst),
            self.get_rtt(),
            self.0
                .packets_sent
                .load(std::sync::atomic::Ordering::SeqCst),
            self.0
                .packets_received
                .load(std::sync::atomic::Ordering::SeqCst),
            jitter,
            self.0.log_name.read().unwrap(),
        )
        .unwrap();
    }
}
