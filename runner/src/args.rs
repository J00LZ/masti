use std::path::PathBuf;

use clap::{Parser, ValueEnum};

#[derive(Debug, ValueEnum, Clone, Copy)]
pub enum TransportType {
    Tcp,
    Quic,
    Sctp,
    RakNetReliable,
    RakNetUnreliable,
    DCCP,
}

#[derive(Debug, ValueEnum, Clone, Copy)]
pub enum Side {
    /// Run the client side of the connection, and collect data
    Client,
    /// Run the server side of the connection
    Server,
}

#[derive(Debug, Parser, Clone)]
#[clap(
    name = "MASTI Runner",
    version,
    author = "Julius de Jeu & Vivian Roest"
)]
pub struct RunnerArgs {
    pub side: Side,
    pub transport: TransportType,

    #[clap(short, long, default_value = "127.0.0.1:5000")]
    /// The address to connect to or listen on
    pub address: std::net::SocketAddr,

    #[clap(short, long, default_value = "./logs/test.json")]
    /// The path to the log file
    pub log: PathBuf,

    #[clap(long, default_value = "1")]
    /// The number of times the log should be run
    pub loops: usize,

    #[clap(long, default_value = "./cert")]
    /// The directory containing the certificates, needed for QUIC
    /// 
    /// The directory should contain the following files:
    /// `domain.crt`, `domain.key`
    pub cert_dir: PathBuf,

    #[clap(short, long, default_value = "")]
    /// For the logs, it's added to the CSV line
    pub communication_type: String,

    #[clap(long, default_value = "1")]
    pub clients: usize,
}
