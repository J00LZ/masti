use std::{
    future::Future,
    marker::PhantomData,
    pin::pin,
    sync::{
        atomic::{AtomicBool, AtomicU64, Ordering},
        mpsc::{channel, Receiver, Sender, TryRecvError},
        Arc,
    },
    task::Poll,
    time::{Duration, Instant},
};

use bytes::{Bytes, BytesMut};
use futures::poll;
use postcard::accumulator::CobsAccumulator;
use std::io::Result as IoResult;

use crate::{
    args::RunnerArgs,
    stats::Stats,
    types::{Message, MessageWrapper},
};

pub trait ClientTransport: Sized + Send {
    const NAME: &'static str;

    type Server;

    fn accept(server: &mut Self::Server) -> impl Future<Output = Option<Self>>;

    fn new(args: RunnerArgs) -> impl Future<Output = Self>;

    fn write_message(&mut self, bytes: Bytes) -> impl Future<Output = IoResult<()>> + Send;

    fn read_bytes(&mut self) -> impl Future<Output = IoResult<Bytes>> + Send;

    fn update(&mut self, _duration: Duration) -> impl Future<Output = ()> + Send {
        async {}
    }
}

pub struct ClientImpl<T> {
    _client: PhantomData<T>,
    recv_buffer: Receiver<Message>,
    send_buffer: Sender<Message>,
    now: Arc<AtomicU64>, // In Nanoseconds
    pub stats: Stats,
    connected: Arc<AtomicBool>,
}

unsafe impl<C> Send for ClientImpl<C> {}
unsafe impl<C> Sync for ClientImpl<C> {}

impl<T: 'static + ClientTransport> ClientImpl<T> {
    pub async fn start(args: RunnerArgs) -> Vec<T> {
        let mut clients = vec![];
        for _ in 0..args.clients {
            let client = T::new(args.clone()).await;
            clients.push(client);
        }
        clients
    }

    pub fn with_connection(t: T) -> Self {
        let (recv_input, recv_buffer) = channel();
        let (send_buffer, send_output) = channel();

        let now = Arc::new(AtomicU64::new(0));
        let now_clone = now.clone();
        let send_buffer_clone = send_buffer.clone();
        let connected = Arc::new(AtomicBool::new(true));
        let bool2 = connected.clone();
        let stats = Stats::new(T::NAME);
        let stats2 = stats.clone();
        tokio::task::spawn(async move {
            let mut client = t;
            let mut buffer = BytesMut::new();
            let mut cobs_buf = CobsAccumulator::<1536>::new();
            let now = now_clone;
            let send_buffer = send_buffer_clone;
            let connected = bool2;
            let stats = stats2;
            let mut now_instant = Instant::now();

            'outer: loop {
                let elapsed = now_instant.elapsed();
                now_instant = Instant::now();
                client.update(elapsed).await;

                match send_output.try_recv() {
                    Ok(m) => {
                        let is_ack = matches!(m, Message::Ack(_));
                        let time = now.load(Ordering::SeqCst);
                        let msg = MessageWrapper::new(m, time);
                        // println!("Sent message: {msg:?}");
                        let msg = postcard::to_allocvec_cobs(&msg).unwrap();
                        if !is_ack {
                            stats.sent_packets(1, msg.len());
                        }
                        if let Err(e) = client.write_message(Bytes::from(msg)).await {
                            eprintln!("{e}");
                            connected.store(false, Ordering::SeqCst);
                            break;
                        }
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => {
                        connected.store(false, Ordering::SeqCst);
                        break;
                    }
                }

                match poll!(pin!(client.read_bytes())) {
                    Poll::Ready(msg) => {
                        if let Ok(msg) = msg {
                            buffer.extend_from_slice(&msg);
                            stats.bytes_received(msg.len());
                        } else {
                            connected.store(false, Ordering::SeqCst);
                            break;
                        }
                    }
                    Poll::Pending => {}
                }
                let mut buf = &buffer[..];
                let remaining: &[u8] = 'cobs: loop {
                    match cobs_buf.feed::<MessageWrapper>(buf) {
                        postcard::accumulator::FeedResult::Consumed => break 'cobs &[],
                        postcard::accumulator::FeedResult::OverFull(f) => {
                            break 'cobs f;
                        }
                        postcard::accumulator::FeedResult::DeserError(f) => {
                            break 'cobs f;
                        }
                        postcard::accumulator::FeedResult::Success {
                            data: msg,
                            remaining,
                        } => {
                            let (msg, send_time) = msg.destructure();
                            if let Message::Ack(time) = msg {
                                let t = now.load(Ordering::SeqCst);
                                let diff = t - time;
                                stats.rtt(diff);
                            } else {
                                stats.received_packet();
                                if let Err(e) = recv_input.send(msg) {
                                    eprintln!("{e}");
                                    connected.store(false, Ordering::SeqCst);
                                    break 'outer;
                                }
                                let ack = Message::Ack(send_time);
                                if let Err(e) = send_buffer.send(ack) {
                                    eprintln!("{e}");
                                    connected.store(false, Ordering::SeqCst);
                                    break 'outer;
                                }
                            }

                            buf = remaining;
                        }
                    }
                };
                buffer = bytes::BytesMut::from(remaining);
                tokio::task::yield_now().await;
            }
        });

        Self {
            _client: PhantomData,
            recv_buffer,
            send_buffer,
            now,
            stats,
            connected,
        }
    }

    pub fn receive_message(&self) -> Option<Message> {
        self.recv_buffer.try_recv().ok()
    }

    pub fn send_message(&self, message: Message) {
        self.send_buffer.send(message).unwrap()
    }

    pub fn update(&self, duration: Duration) {
        self.now
            .fetch_add(duration.as_nanos() as u64, Ordering::SeqCst);
    }

    pub fn is_connected(&self) -> bool {
        self.connected.load(Ordering::SeqCst)
    }
}
