use std::time::Duration;

use bevy::ecs::system::Resource;

use crate::transports::{Client, Server, TransportError};

pub trait Transport: Send + Sync + Resource {
    type Server: Server;

    /// Advances the transport by the duration, and receive packets from the network.
    fn update(
        &mut self,
        duration: Duration,
        server: &mut Self::Server,
    ) -> Result<(), TransportError>;

    /// Send packets to connected clients.
    fn send_packets(&mut self, server: &mut Self::Server);

    /// Disconnects all connected clients.
    /// This sends the disconnect packet instantly, use this when closing/exiting games,
    fn disconnect_all(&mut self, server: &mut Self::Server);
}

pub trait ClientTransport: Send + Sync + Resource {
    type Client: Client;

    fn update(
        &mut self,
        duration: Duration,
        client: &mut Self::Client,
    ) -> Result<(), TransportError>;

    fn send_packets(&mut self, client: &mut Self::Client) -> Result<(), TransportError>;

    fn disconnect(&mut self);
}
