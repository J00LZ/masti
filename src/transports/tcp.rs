pub mod client {
    use std::{
        collections::{HashMap, VecDeque}, io::{ErrorKind, Read, Write}, net::TcpStream, sync::atomic::AtomicUsize, time::{Duration, SystemTime, UNIX_EPOCH}
    };

    use bevy::{
        app::Update,
        ecs::{schedule::IntoSystemSetConfigs, system::Resource},
        log::warn,
    };

    use bytes::Bytes;

    use crate::{transports::{
        channels::ChannelMessage, client_connected, headless_traits::HeadlessClient,
        plugins::ClientPlugin, stats::Stats, Client, ClientId, Connected, TransportError,
    }, ACK_CHANNEL};
    pub static OOF: AtomicUsize = AtomicUsize::new(0);
    #[derive(Debug, Resource)]
    pub struct TcpClient {
        pub id: ClientId,
        pub socket: TcpStream,
        pub buffer: HashMap<u8, VecDeque<Vec<u8>>>,
        pub rtt: f64,
        pub stats: Stats,
        pub current_time: Duration,
        pub connected: bool,
    }

    impl Client for TcpClient {
        fn init(_id: u64) -> Self {
            let socket = TcpStream::connect("127.0.0.1:5000").unwrap();
            socket.set_nonblocking(true).unwrap();

            Self {
                socket,
                id: ClientId(0),
                buffer: HashMap::new(),
                rtt: 0.0,
                stats: Stats::new(),
                current_time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
                connected: true,
            }
        }

        fn init_bevy(app: &mut bevy::prelude::App, id: u64) {
            app.add_event::<TransportError>();
            app.add_plugins(ClientPlugin::<Self>::default());
            app.configure_sets(Update, Connected.run_if(client_connected::<Self>));

            let client = Self::init(id);

            app.insert_resource(client);
        }

        fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<bytes::Bytes> {
            self.buffer
                .get_mut(&channel.into())
                .and_then(|v| v.pop_front())
                .map(Into::into)
        }

        fn send_message<I: Into<u8>, B: Into<bytes::Bytes>>(&mut self, channel_id: I, message: B) {
            let msg = ChannelMessage::new(channel_id.into(), message.into(), self.current_time);
            let msg = bincode::serialize(&msg).unwrap();
            self.stats.sent_packets(1, msg.len());
            if let Err(e) = self.socket.write_all(&msg) {
                if e.kind() == ErrorKind::ConnectionReset {
                    self.connected = false;
                } else {
                    warn!("Error sending message: {}", e);
                }
            }
        }

        fn update(&mut self, duration: std::time::Duration) {
            self.current_time += duration;

            let mut buf = [42; 1500];
            let n = match self.socket.read(&mut buf) {
                Ok(el) => el,
                Err(e) if e.kind() == ErrorKind::WouldBlock => 0,
                Err(e) if e.kind() == ErrorKind::ConnectionReset => {
                    self.connected = false;
                    return;
                }
                e => e.unwrap(),
            };
            let mut buf = &buf[..n];

            if n > 0 {
                self.stats.bytes_received(n);
                while let Ok(msg) = bincode::deserialize_from::<_, ChannelMessage>(&mut buf) {
                    if msg.channel == ACK_CHANNEL {
                        self.stats
                            .rtt(self.current_time.saturating_sub(msg.send_time).as_nanos() as u64);
                    } else {
                        let ack = ChannelMessage::new(42, Vec::new().into(), msg.send_time);
                        let msg = bincode::serialize(&ack).unwrap();
                        self.stats.sent_packets(1, msg.len());
                        if let Err(e) = self.socket.write_all(&msg) {
                            if e.kind() == ErrorKind::ConnectionReset {
                                self.connected = false;
                            } else {
                                warn!("Error sending message: {}", e);
                            }
                        }
                    }

                    // Update rtt
                    self.buffer
                        .entry(msg.channel)
                        .or_default()
                        .push_back(msg.message);
                }
            }
        }

        fn is_connected(&self) -> bool {
            self.connected
        }

        fn set_client_id(&mut self, id: ClientId) {
            self.id = id;
        }
    }

    impl HeadlessClient for TcpClient {
        async fn new(id: ClientId, addr: impl std::net::ToSocketAddrs) -> Self {
            let socket = TcpStream::connect(addr).unwrap();
            socket.set_nonblocking(true).unwrap();

            Self {
                id,
                socket,
                buffer: HashMap::new(),
                rtt: 0.0,
                stats: Stats::new(),
                current_time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
                connected: true,
            }
        }

        async fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<Bytes> {
            self.buffer
                .get_mut(&channel.into())
                .and_then(|v| v.pop_front())
                .map(Into::into)
        }

        async fn send_message< B: Into<Bytes>>(&mut self, channel_id: u8, message: B) {
            let msg = ChannelMessage::new(channel_id, message.into(), self.current_time);
            let msg = bincode::serialize(&msg).unwrap();
            self.stats.sent_packets(1, msg.len());
            OOF.fetch_add(1, std::sync::atomic::Ordering::SeqCst);
            if let Err(e) = self.socket.write_all(&msg) {
                if e.kind() == ErrorKind::ConnectionReset {
                    self.connected = false;
                } else {
                    warn!("Error sending message: {}", e);
                }
            }
        }

        async fn update(&mut self, duration: std::time::Duration) {
            self.current_time += duration;

            let mut buf = [42; 1500];
            let n = match self.socket.read(&mut buf) {
                Ok(el) => el,
                Err(e) if e.kind() == ErrorKind::WouldBlock => 0,
                Err(e) if e.kind() == ErrorKind::ConnectionReset => {
                    self.connected = false;
                    return;
                }
                e => e.unwrap(),
            };
            let mut buf = &buf[..n];

            if n > 0 {
                self.stats.bytes_received(n);
                while let Ok(msg) = bincode::deserialize_from::<_, ChannelMessage>(&mut buf) {
                    if msg.channel == 42 {
                        self.stats
                            .rtt(self.current_time.saturating_sub(msg.send_time).as_nanos() as u64);
                    } else {
                        let ack = ChannelMessage::new(42, Vec::new().into(), msg.send_time);
                        let msg = bincode::serialize(&ack).unwrap();
                        // self.stats.sent_packets(1, msg.len());
                        if let Err(e) = self.socket.write_all(&msg) {
                            if e.kind() == ErrorKind::ConnectionReset {
                                self.connected = false;
                            } else {
                                warn!("Error sending message: {}", e);
                            }
                        }
                        self.stats.received_packet(); 
                    }


                    self.buffer
                        .entry(msg.channel)
                        .or_default()
                        .push_back(msg.message);
                }
            }
        }

        fn is_connected(&self) -> bool {
            self.connected
        }
    }
}

pub mod server {
    use std::{
        collections::{HashMap, VecDeque}, fs::{File, OpenOptions}, io::{ErrorKind, Write}, net::TcpListener, time::{Duration, SystemTime, UNIX_EPOCH}
    };

    use bevy::{
        app::{PostUpdate, PreUpdate},
        ecs::{
            schedule::{common_conditions::resource_exists, IntoSystemConfigs},
            system::Resource,
        },
    };
    use bevy_renet::{renet::ServerEvent, RenetReceive};

    use crate::{
        transports::{
            channels::ChannelMessage, headless_traits::HeadlessServer, plugins::ServerPlugin,
            stats::Stats, Client, ClientId, Server, TransportError,
        },
        ServerChannel, ServerMessages,
    };

    use super::client::TcpClient;

    #[derive(Debug, Resource)]
    pub struct TcpServer {
        socket: std::net::TcpListener,
        clients: HashMap<ClientId, TcpClient>,
        events: VecDeque<ServerEvent>,
        current_time: Duration,
    }

    impl Server for TcpServer {
        fn init_bevy(app: &mut bevy::prelude::App) {
            // app.add_plugins(TransportLayerServerPlugin::<Self, Self>::default());
            app.add_plugins(ServerPlugin::<Self>::default());
            app.add_event::<TransportError>();

            app.add_systems(
                PreUpdate,
                Self::update_system
                    .in_set(RenetReceive)
                    .run_if(resource_exists::<Self>),
            );

            app.add_systems(
                PostUpdate,
                (Self::disconnect_on_exit,)
                    .run_if(resource_exists::<Self>)
                    .after(Self::update_system),
            );

            let socket = TcpListener::bind("127.0.0.1:5000").unwrap();
            socket.set_nonblocking(true).unwrap();

            app.insert_resource(Self {
                socket,
                clients: HashMap::default(),
                events: VecDeque::new(),
                current_time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
            });
        }

        fn broadcast_message<I: Into<u8>, B: Into<bytes::Bytes>>(
            &mut self,
            channel_id: I,
            message: B,
        ) {
            let message = message.into();
            let channel_id = channel_id.into();
            for (_, c) in self.clients.iter_mut() {
                c.send_message(channel_id, message.clone());
            }
        }

        fn receive_message<I: Into<u8>>(
            &mut self,
            client_id: crate::transports::ClientId,
            channel_id: I,
        ) -> Option<bytes::Bytes> {
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.receive_message(channel_id)
            } else {
                None
            }
        }

        fn send_message<I: Into<u8>, B: Into<bytes::Bytes>>(
            &mut self,
            client_id: crate::transports::ClientId,
            channel_id: I,
            message: B,
        ) {
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.send_message(channel_id, message);
            }
        }

        fn clients_id(&self) -> Vec<crate::transports::ClientId> {
            self.clients.keys().cloned().collect()
        }

        fn update(&mut self, duration: std::time::Duration) {
            self.current_time += duration;
            match self.socket.accept() {
                Ok((mut socket, _)) => {
                    let id = ClientId::new();
                    let message = bincode::serialize(&ServerMessages::Helo { id }).unwrap();
                    let message = ChannelMessage::new(
                        ServerChannel::ServerMessages,
                        message.into(),
                        self.current_time,
                    );
                    let message = bincode::serialize(&message).unwrap();
                    println!("Client with {message:?} connecting");
                    socket.write_all(&message).unwrap();
                    self.clients.insert(
                        id,
                        TcpClient {
                            id,
                            socket,
                            buffer: HashMap::new(),
                            rtt: 0.0,
                            stats: Stats::new(),
                            current_time: self.current_time,
                            connected: true,
                        },
                    );
                    self.events.push_back(ServerEvent::ClientConnected {
                        client_id: id.into(),
                    });
                }
                Err(e) if e.kind() == ErrorKind::WouldBlock => {}
                Err(e) => panic!("{}", e),
            }
            let mut to_remove = vec![];
            for (_, c) in self.clients.iter_mut() {
                c.update(duration);
                if !c.connected {
                    to_remove.push(c.id);
                    self.events.push_back(ServerEvent::ClientDisconnected {
                        client_id: c.id.into(),
                        reason: bevy_renet::renet::DisconnectReason::DisconnectedByClient,
                    });
                }
            }
            for id in to_remove {
                self.clients.remove(&id);
            }
        }

        fn get_event(&mut self) -> Option<bevy_renet::renet::ServerEvent> {
            self.events.pop_front()
        }

        fn network_info(&self, client_id: ClientId) -> Option<bevy_renet::renet::NetworkInfo> {
            self.clients.get(&client_id).map(|c| c.network_info())
        }
    }

    impl HeadlessServer for TcpServer {
        async fn new(addr: impl std::net::ToSocketAddrs) -> Self {
            let socket = TcpListener::bind(addr).unwrap();
            socket.set_nonblocking(true).unwrap();

            Self {
                socket,
                clients: HashMap::default(),
                events: VecDeque::new(),
                current_time: SystemTime::now().duration_since(UNIX_EPOCH).unwrap(),
            }
        }

        async fn broadcast_message<B: Into<bytes::Bytes>>(
            &mut self,
            channel_id: u8,
            message: B,
        ) {
            self.remove_disconnected();
            let message = message.into();
            for (_, c) in self.clients.iter_mut() {
                c.send_message(channel_id, message.clone());
            }
        }

        async fn receive_message<I: Into<u8>>(
            &mut self,
            client_id: ClientId,
            channel_id: I,
        ) -> Option<bytes::Bytes> {
            self.remove_disconnected();
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.receive_message(channel_id)
            } else {
                None
            }
        }

        async fn send_message<B: Into<bytes::Bytes>>(
            &mut self,
            client_id: ClientId,
            channel_id: u8,
            message: B,
        ) {
            self.remove_disconnected();
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.send_message(channel_id, message);
            }
        }

        fn client_ids(&self) -> Vec<ClientId> {
            self.clients.keys().cloned().collect()
        }

        async fn update(&mut self, duration: std::time::Duration) {
            self.current_time += duration;
            self.remove_disconnected();
            match self.socket.accept() {
                Ok((mut socket, _)) => {
                    let id = ClientId::new();
                    let message = bincode::serialize(&ServerMessages::Helo { id }).unwrap();
                    let message = ChannelMessage::new(
                        ServerChannel::ServerMessages,
                        message.into(),
                        self.current_time,
                    );
                    let message = bincode::serialize(&message).unwrap();
                    // println!("Client with {message:?} connecting");
                    socket.write_all(&message).unwrap();
                    self.clients.insert(
                        id,
                        TcpClient {
                            id,
                            socket,
                            buffer: HashMap::new(),
                            rtt: 0.0,
                            stats: Stats::new(),
                            current_time: self.current_time,
                            connected: true,
                        },
                    );
                    self.events.push_back(ServerEvent::ClientConnected {
                        client_id: id.into(),
                    });
                }
                Err(e) if e.kind() == ErrorKind::WouldBlock => {}
                Err(e) => panic!("{}", e),
            }

            for (_, c) in self.clients.iter_mut() {
                c.update(duration);
            }
        }

        fn get_event(&mut self) -> Option<ServerEvent> {
            self.events.pop_front()
        }
    }

    impl TcpServer {
        fn remove_disconnected(&mut self) {
            let mut to_remove = vec![];
            for (_, c) in self.clients.iter_mut() {
                if !c.connected {
                    to_remove.push(c.id);
                    self.events.push_back(ServerEvent::ClientDisconnected {
                        client_id: c.id.into(),
                        reason: bevy_renet::renet::DisconnectReason::DisconnectedByClient,
                    });
                }
            }
            for id in to_remove {
                if let Some(client)  = self.clients.remove(&id) {
                    println!("stats for {:?}: {:?}", id, client.stats);
                }
            }
        }
    }
    pub struct AsyncTcpServer {}
}
