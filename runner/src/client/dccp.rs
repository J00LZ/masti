use dccp::DCCPSocket;

use crate::{args::RunnerArgs, server::dccp::DCCPTomfoolery};

use super::impls::ClientTransport;

impl ClientTransport for DCCPSocket {
    const NAME: &'static str = "dccp";

    type Server = DCCPTomfoolery;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        server.accept()
    }

    async fn new(args: RunnerArgs) -> Self {
        DCCPSocket::connect(args.address, 0x3F343230).unwrap()
    }

    async fn write_message(&mut self, bytes: bytes::Bytes) -> std::io::Result<()> {
        self.send(&bytes).unwrap();
        Ok(())
    }

    async fn read_bytes(&mut self) -> std::io::Result<bytes::Bytes> {
        let mut buf = vec![0; 1500];
        let len = self.recv(&mut buf).unwrap();
        Ok(bytes::Bytes::copy_from_slice(&buf[..len]))
    }
}
