use crate::args::RunnerArgs;

use super::impls::ServerTransport;

impl ServerTransport for rust_raknet::RaknetListener {
    async fn new(args: RunnerArgs) -> Self {
        let mut listener = rust_raknet::RaknetListener::bind(&args.address)
            .await
            .unwrap();
        listener.listen().await;
        listener
    }
}
