pub mod client {
    use std::{
        collections::{HashMap, VecDeque},
        net::{SocketAddr, ToSocketAddrs},
        path::Path,
        pin::pin,
        sync::{atomic::AtomicBool, mpsc::Sender, Arc, Mutex},
        time::{Duration, Instant, SystemTime, UNIX_EPOCH},
    };

    use bevy::log::warn;
    use bevy_renet::renet::ServerEvent;
    use bytes::{Bytes, BytesMut};

    use futures::poll;
    use postcard::accumulator::CobsAccumulator;
    use s2n_quic::{client::Connect, stream::BidirectionalStream, Client, Connection};
    use tokio::sync::RwLock;

    use crate::{
        transports::{
            channels::ChannelMessage, headless_traits::HeadlessClient, stats::Stats,
            transport::ClientTransport, ClientId,
        },
        ClientChannel, PlayerInput, ServerChannel, ServerMessages, ACK_CHANNEL,
    };

    pub type ActualQuicClient = (Option<Client>, Connection, BidirectionalStream);

    pub struct QuicClient {
        pub id: ClientId,
        pub connection: Connection,
        pub buffer: Arc<Mutex<HashMap<u8, VecDeque<Vec<u8>>>>>,
        pub rtt: f64,
        pub stats: Stats,
        pub current_time: Arc<RwLock<Duration>>,
        pub connected: Arc<AtomicBool>,
        pub sender: Sender<Bytes>,
    }

    impl HeadlessClient for QuicClient {
        async fn new(id: ClientId, addr: impl std::net::ToSocketAddrs) -> Self {
            let client = Client::builder()
                .with_tls(Path::new("./cert/domain.crt"))
                .unwrap()
                .with_io("0.0.0.0:0")
                .unwrap()
                .start()
                .unwrap();

            let addr = addr.to_socket_addrs().unwrap().next().unwrap();
            let socket = Connect::new(addr).with_server_name("localhost");
            let connection = client.connect(socket).await.unwrap();
            Self::with_connection(id, connection, true).await
        }

        async fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<Bytes> {
            self.buffer
                .lock()
                .unwrap()
                .get_mut(&channel.into())
                .and_then(|v| v.pop_front())
                .map(Into::into)
        }

        async fn send_message<B: Into<Bytes>>(&mut self, channel_id: u8, message: B) {
            let t = *self.current_time.read().await;
            let msg = message.into();
            // println!("Sending message on channel {}", channel_id);
            if channel_id == 0 {
                println!("{msg:?}");
                unreachable!("Channel not implemented")
            } else if channel_id == ClientChannel::Input as u8 {
                let res = bincode::deserialize::<PlayerInput>(&msg).unwrap();
                println!("sending message: {:?}", res);
            } else if channel_id == ServerChannel::ServerMessages as u8 {
                let res = bincode::deserialize::<ServerMessages>(&msg).unwrap();
                println!("sending message: {:?}", res);
            }
            let msg = ChannelMessage::new(channel_id, msg, t);
            let msg = postcard::to_allocvec_cobs(&msg).unwrap();
            if channel_id != ACK_CHANNEL {
                self.stats.sent_packets(1, msg.len());
            }

            if let Err(e) = self.sender.send(msg.into()) {
                self.connected
                    .store(false, std::sync::atomic::Ordering::Release);
                warn!("Error sending message: {}", e);
            }
        }

        async fn update(&mut self, duration: std::time::Duration) {
            // println!("Updating client {:?}", self.id);
            let mut guard = self.current_time.write().await;
            *guard += duration;
        }

        fn is_connected(&self) -> bool {
            self.connected.load(std::sync::atomic::Ordering::Acquire)
        }
    }

    impl QuicClient {
        pub(super) async fn with_connection(
            id: ClientId,
            mut connection: Connection,
            open: bool,
        ) -> Self {
            connection.keep_alive(true).unwrap();
            let stream = if open {
                connection.open_bidirectional_stream().await.unwrap()
            } else {
                connection
                    .accept_bidirectional_stream()
                    .await
                    .unwrap()
                    .unwrap()
            };
            let (mut r, mut send_stream) = stream.split();
            let stats = Stats::new();
            let buffer: Arc<Mutex<HashMap<u8, VecDeque<Vec<u8>>>>> =
                Arc::new(Mutex::new(HashMap::new()));
            let buffer2 = buffer.clone();
            let c = Arc::new(AtomicBool::new(true));
            let connected = c.clone();
            let (tx, rx) = std::sync::mpsc::channel();
            tokio::spawn(async move {
                let mut x = 0;
                let mut y = 0usize;
                while let Ok(msg) = rx.recv() {
                    y += 1;
                    send_stream.send(msg).await.unwrap();
                    x += 1;
                    if x > 10 {
                        if let Err(e) = send_stream.flush().await {
                            warn!("Error flushing stream: {}", e);
                            c.store(false, std::sync::atomic::Ordering::Release);
                            break;
                        }
                        x = 0;
                        println!("Sent {} messages", y);
                    }
                }
                println!("Sent {} messages", y);
            });

            let current_time = Arc::new(RwLock::new(
                SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap(),
            ));

            let now = current_time.clone();

            let tx2 = tx.clone();
            let stats2 = stats.clone();

            tokio::spawn(async move {
                let mut foobuf = BytesMut::new();
                let buffer = buffer.clone();
                let mut cobs_buf = CobsAccumulator::<1536>::new();
                let stats = stats2;
                let mut ack_count = 0;
                loop {
                    match r.receive().await {
                        Ok(None) => {}
                        Ok(Some(el)) => {
                            stats.bytes_received(el.len());
                            foobuf.extend_from_slice(&el);
                        }

                        e => {
                            if let Err(e) = e {
                                // self.connected = false;

                                warn!("Error receiving message: {}", e);
                            }
                            continue;
                        }
                    };

                    if !foobuf.is_empty() {
                        // self.stats.received_packet(n as u64);
                        // println!("zeroes: {}", foobuf.iter().filter(|&&b| b == 0).count());
                        // println!("Received {} bytes", foobuf.len());
                        let buf = foobuf.clone();
                        let mut buf = &buf[..];

                        'cobs: loop {
                            match cobs_buf.feed::<ChannelMessage>(buf) {
                                postcard::accumulator::FeedResult::Consumed => break 'cobs,
                                postcard::accumulator::FeedResult::OverFull(f) => {
                                    foobuf = BytesMut::from(f);
                                    break 'cobs;
                                }
                                postcard::accumulator::FeedResult::DeserError(f) => {
                                    foobuf = BytesMut::from(f);
                                    break 'cobs;
                                }
                                postcard::accumulator::FeedResult::Success {
                                    data: msg,
                                    remaining,
                                } => {
                                    if msg.channel == ACK_CHANNEL {
                                        let t = *now.read().await;
                                        // TODO: Is this correct?
                                        stats.rtt((t - msg.send_time).as_nanos() as u64);
                                    } else {
                                        stats.received_packet();
                                        println!("Received message on channel {}", msg.channel);
                                        buffer
                                            .lock()
                                            .unwrap()
                                            .entry(msg.channel)
                                            .or_default()
                                            .push_back(msg.message);
                                        // println!("Received message on channel {}", msg.channel);
                                        let ack = ChannelMessage::new(
                                            ACK_CHANNEL,
                                            Bytes::new(),
                                            msg.send_time,
                                        );
                                        let msg = postcard::to_allocvec_cobs(&ack).unwrap();
                                        ack_count += 1;
                                        if let Err(e) = tx2.send(msg.into()) {
                                            // self.connected = false;
                                            warn!("Error sending message: {}", e);
                                        }
                                    }

                                    buf = remaining;
                                }
                            }
                        }
                    }
                }
            });

            Self {
                id,
                connection,
                sender: tx,
                rtt: 0.0,
                stats,
                current_time,
                connected,
                buffer: buffer2,
            }
        }
    }

    impl ClientTransport for BidirectionalStream {
        type Server = s2n_quic::Server;

        async fn accept(server: &mut Self::Server) -> Option<Self> {
            let f = server.accept();
            match poll!(pin!(f)) {
                std::task::Poll::Ready(c) => {
                    let mut c = c?;
                    let s = c.accept_bidirectional_stream().await.ok()??;
                    println!("Client connected");
                    Some(s)
                }
                std::task::Poll::Pending => None,
            }
        }

        async fn new(ip: &str) -> Self {
            let client = Client::builder()
                .with_tls(Path::new("./cert/domain.crt"))
                .unwrap()
                .with_io("0.0.0.0:0")
                .unwrap()
                .start()
                .unwrap();

            let a = ip.to_socket_addrs().unwrap().next().unwrap();
            let socket = Connect::new(a).with_server_name("localhost");
            let mut connection = client.connect(socket).await.unwrap();
            let s = connection.open_bidirectional_stream().await.unwrap();
            s
        }

        async fn write_message(&mut self, bytes: Bytes) -> std::io::Result<()> {
            self.send(bytes).await?;
            // match poll!(pin!(self.2.flush())) {
            //     std::task::Poll::Ready(_) => {}
            //     std::task::Poll::Pending => {}
            // }

            Ok(())
        }

        async fn read_bytes(&mut self) -> std::io::Result<Bytes> {
            let n = self.receive().await?.ok_or_else(|| {
                std::io::Error::new(std::io::ErrorKind::ConnectionAborted, "Connection closed")
            })?;
            println!("Read {} bytes", n.len());
            Ok(n)
        }
    }
}

pub mod server {
    use std::{
        collections::{HashMap, VecDeque},
        net::ToSocketAddrs,
        path::Path,
        pin::pin,
        time::{Duration, SystemTime},
    };

    use bevy_renet::renet::ServerEvent;
    use futures::poll;
    use s2n_quic::Server;

    use crate::{
        transports::{
            headless_traits::{HeadlessClient, HeadlessServer},
            transport::ServerTransport,
            ClientId,
        },
        ServerChannel, ServerMessages,
    };

    use super::client::QuicClient;

    pub struct QuicServer {
        socket: Server,
        events: VecDeque<ServerEvent>,
        pub clients: HashMap<ClientId, QuicClient>,
        current_time: Duration,
    }

    impl HeadlessServer for QuicServer {
        async fn new(addr: impl std::net::ToSocketAddrs) -> Self {
            let addr = addr.to_socket_addrs().unwrap().next().unwrap();
            let socket = Server::builder()
                .with_tls((
                    Path::new("./cert/domain.crt"),
                    Path::new("./cert/domain.key"),
                ))
                .unwrap()
                .with_io(addr)
                .unwrap()
                .start();
            let socket = match socket {
                Ok(s) => s,
                Err(e) => {
                    panic!("Error starting server: {}", e);
                }
            };
            Self {
                socket,
                clients: HashMap::default(),
                events: VecDeque::new(),
                current_time: SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap(),
            }
        }

        async fn broadcast_message<B: Into<bytes::Bytes>>(&mut self, channel_id: u8, message: B) {
            self.remove_disconnected();
            let message = message.into();
            for (_, c) in self.clients.iter_mut() {
                c.send_message(channel_id, message.clone()).await;
            }
        }

        async fn receive_message<I: Into<u8>>(
            &mut self,
            client_id: ClientId,
            channel_id: I,
        ) -> Option<bytes::Bytes> {
            self.remove_disconnected();
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.receive_message(channel_id).await
            } else {
                None
            }
        }

        async fn send_message<B: Into<bytes::Bytes>>(
            &mut self,
            client_id: ClientId,
            channel_id: u8,
            message: B,
        ) {
            self.remove_disconnected();
            if let Some(client) = self.clients.get_mut(&client_id) {
                client.send_message(channel_id, message).await;
            }
        }

        fn client_ids(&self) -> Vec<ClientId> {
            self.clients.keys().cloned().collect()
        }

        async fn update(&mut self, duration: std::time::Duration) {
            self.current_time += duration;
            self.remove_disconnected();
            let f = self.socket.accept();
            match poll!(pin!(f)) {
                std::task::Poll::Ready(sock) => {
                    if let Some(socket) = sock {
                        let id = ClientId::new();
                        let mut client = QuicClient::with_connection(id, socket, false).await;
                        // let mut stream = socket.open_bidirectional_stream().await.unwrap();
                        let message = bincode::serialize(&ServerMessages::Helo { id }).unwrap();
                        // let message = ChannelMessage::new(
                        //     ServerChannel::ServerMessages,
                        //     message.into(),
                        //     self.current_time,
                        // );
                        println!("Client with {message:?} connecting");
                        // println!("length is {}", message.len());
                        // let message = bincode::serialize(&message).unwrap();
                        // stream.send(message.into()).await.unwrap();
                        // stream.flush().await.unwrap();
                        client
                            .send_message(ServerChannel::ServerMessages as u8, message)
                            .await;
                        self.clients.insert(id, client);
                        self.events.push_back(ServerEvent::ClientConnected {
                            client_id: id.into(),
                        });
                    }
                }
                std::task::Poll::Pending => {}
            }

            for (_, c) in self.clients.iter_mut() {
                c.update(duration).await;
            }
        }

        fn get_event(&mut self) -> Option<ServerEvent> {
            self.events.pop_front()
        }
    }

    impl QuicServer {
        fn remove_disconnected(&mut self) {
            let mut to_remove = vec![];
            // println!("Checking for disconnected clients");
            for (_, c) in self.clients.iter_mut() {
                // println!("Checking client {}", c.id.0);
                if !c.connected.load(std::sync::atomic::Ordering::Acquire) {
                    println!("Client {} disconnected", c.id.0);
                    to_remove.push(c.id);
                    self.events.push_back(ServerEvent::ClientDisconnected {
                        client_id: c.id.into(),
                        reason: bevy_renet::renet::DisconnectReason::DisconnectedByClient,
                    });
                }
            }
            for id in to_remove {
                println!("Removing client {}", id.0);
                if let Some(client) = self.clients.remove(&id) {
                    println!("Stats for {}: {:?}", id.0, client.stats);
                }
            }
        }
    }

    impl ServerTransport for Server {
        const NAME: &'static str = "QUIC";

        async fn new(addr: &str) -> Self {
            let addr = addr.to_socket_addrs().unwrap().next().unwrap();
            let socket = Server::builder()
                .with_tls((
                    Path::new("./cert/domain.crt"),
                    Path::new("./cert/domain.key"),
                ))
                .unwrap()
                .with_io(addr)
                .unwrap()
                .start();
            let socket = match socket {
                Ok(s) => s,
                Err(e) => {
                    panic!("Error starting server: {}", e);
                }
            };
            socket
        }
    }
}
