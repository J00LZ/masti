use std::collections::HashMap;

use serde::{Deserialize, Serialize};

#[derive(Debug, Deserialize, Serialize, PartialEq, PartialOrd, Default)]
struct Entry {
    proto: String,
    connection_type: String,
    size: String,
    bytes_sent: u64,
    bytes_received: u64,
    avg_rtt_ns: f64,
    packets_sent: u64,
    packets_recv: u64,
    jitter_ns: f64,
}

impl Eq for Entry {}

impl Ord for Entry {
    fn cmp(&self, other: &Self) -> std::cmp::Ordering {
        self.proto
            .cmp(&other.proto)
            .then(self.connection_type.cmp(&other.connection_type))
            .then(self.size.cmp(&other.size))
    }
}

fn main() {
    let input = csv::Reader::from_path("./out.csv").unwrap();
    let entries = input
        .into_deserialize()
        .collect::<Result<Vec<Entry>, _>>()
        .unwrap();
    let mut m = HashMap::new();
    for entry in entries {
        m.entry((
            entry.proto.clone(),
            entry.connection_type.clone(),
            entry.size.clone(),
        ))
        .or_insert_with(Vec::new)
        .push(entry);
    }
    let mut new_entries = vec![];
    for v in m.into_values() {
        let mut new_entry = Entry::default();
        let l = v.len() as u64;
        new_entry.proto = v[0].proto.clone();
        new_entry.connection_type = v[0].connection_type.clone();
        new_entry.size = v[0].size.clone();
        for entry in v {
            new_entry.bytes_sent += entry.bytes_sent / l;
            new_entry.bytes_received += entry.bytes_received / l;
            new_entry.avg_rtt_ns += entry.avg_rtt_ns / (l as f64);
            new_entry.packets_sent += entry.packets_sent / l;
            new_entry.packets_recv += entry.packets_recv / l;
            new_entry.jitter_ns += entry.jitter_ns / (l as f64);
        }
        new_entries.push(new_entry);
    }
    new_entries.sort();
    let mut wtr = csv::Writer::from_path("./out_merged.csv").unwrap();
    for entry in new_entries {
        wtr.serialize(entry).unwrap();
    }

}
