use tokio::net::TcpListener;

use crate::args::RunnerArgs;

use super::impls::ServerTransport;

impl ServerTransport for TcpListener {
    async fn new(args: RunnerArgs) -> Self {
        TcpListener::bind(args.address).await.unwrap()
    }
}
