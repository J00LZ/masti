//! This module contains all the various transport modes we support
//!
//!

use std::fmt::{Display, Formatter};
use std::time::{Duration, SystemTime, UNIX_EPOCH};

use bevy::{
    app::{App, AppExit},
    ecs::{
        event::{Event, EventReader},
        schedule::SystemSet,
        system::{Res, ResMut, Resource},
    },
    time::Time,
};
use bevy_renet::renet::{ClientId as RenetId, NetworkInfo, ServerEvent};
use bytes::Bytes;
use serde::{Deserialize, Serialize};

pub mod plugins;

/// Unique identifier for clients.
#[derive(
Debug, Clone, Copy, Hash, PartialEq, Eq, Ord, PartialOrd, Resource, Serialize, Deserialize,
)]
pub struct ClientId(pub u64);

impl Display for ClientId {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

#[derive(SystemSet, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct Connected;

pub fn client_connected<C: Client>(client: Option<Res<C>>) -> bool {
    match client {
        Some(client) => client.is_connected(),
        None => false,
    }
}

impl ClientId {
    pub fn new() -> Self {
        ClientId(
            SystemTime::now()
                .duration_since(UNIX_EPOCH)
                .unwrap()
                .as_millis() as u64,
        )
    }
}

impl Default for ClientId {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Event)]
pub enum TransportError {}

impl From<RenetId> for ClientId {
    fn from(value: RenetId) -> Self {
        Self(value.raw())
    }
}

impl From<ClientId> for RenetId {
    fn from(val: ClientId) -> Self {
        RenetId::from_raw(val.0)
    }
}

pub trait Client: Resource {
    fn init(id: u64) -> Self;

    /// This should add the client to bevy
    fn init_bevy(app: &mut App, id: u64);
    fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<Bytes>;
    fn send_message<I: Into<u8>, B: Into<Bytes>>(&mut self, channel_id: I, message: B);

    /// Advances the client by the duration.
    /// Should be called every tick
    fn update(&mut self, duration: std::time::Duration);

    fn update_transport(&mut self, _duration: Duration) {}

    /// can be used to do post-processing on the client,
    /// should be called after all packets for this tick have been
    /// sent via [`Client::send_message`].
    fn send_packets(&mut self) {}

    fn is_connected(&self) -> bool;

    fn network_info(&self) -> NetworkInfo {
        NetworkInfo {
            rtt: 1.0,
            packet_loss: 0.0,
            bytes_sent_per_second: 0.0,
            bytes_received_per_second: 0.0,
        }
    }

    fn set_client_id(&mut self, _id: ClientId) {}
}

pub trait Server: Resource {
    fn init_bevy(app: &mut App);
    fn broadcast_message<I: Into<u8>, B: Into<Bytes>>(&mut self, channel_id: I, message: B);
    fn receive_message<I: Into<u8>>(&mut self, client_id: ClientId, channel_id: I)
                                    -> Option<Bytes>;
    fn send_message<I: Into<u8>, B: Into<Bytes>>(
        &mut self,
        client_id: ClientId,
        channel_id: I,
        message: B,
    );
    fn clients_id(&self) -> Vec<ClientId>;

    /// Advances the server by the duration.
    /// Should be called every tick
    fn update(&mut self, duration: Duration);

    /// Returns a server event if available
    fn get_event(&mut self) -> Option<ServerEvent>;

    fn update_system(mut transport: ResMut<Self>, time: Res<Time>) {
        transport.update(time.delta())
    }

    fn disconnect_on_exit(_exit: EventReader<AppExit>, _transport: ResMut<Self>) {}

    fn network_info(&self, _client_id: ClientId) -> Option<NetworkInfo> {
        None
    }
}

pub mod headless_traits;

pub mod channels;
pub mod stats;

pub mod quic;
/// The renet protocol
pub mod renet;
pub mod tcp;
pub mod transport;
