pub const PROTOCOL_ID: u64 = 7;

pub mod client {
    use std::net::UdpSocket;
    use std::time::{Duration, SystemTime};

    use bevy::ecs::event::EventReader;
    use bevy::ecs::system::Resource;

    use bevy::app::{App, Update};
    use bevy::prelude::IntoSystemSetConfigs;
    use bevy_renet::renet::transport::{
        ClientAuthentication, NetcodeClientTransport, NetcodeTransportError,
    };
    // use bevy_renet::RenetClientPlugin;
    use bevy_renet::renet::RenetClient;

    use crate::connection_config;
    use crate::transports::headless_traits::HeadlessClient;
    use crate::transports::plugins::ClientPlugin;
    use crate::transports::renet::PROTOCOL_ID;
    use crate::transports::stats::Stats;
    use crate::transports::{client_connected, Client, ClientId, Connected};

    #[derive(Resource, Debug)]
    pub struct RenetClientShim {
        pub client: RenetClient,
        pub transport: NetcodeClientTransport,
        pub stats: Stats,
    }

    impl Client for RenetClientShim {
        fn init(id: u64) -> Self {
            let client = RenetClient::new(connection_config());
            let server_addr = "127.0.0.1:5000".parse().unwrap();
            let socket = UdpSocket::bind("127.0.0.1:0").unwrap();

            let current_time = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            let authentication = ClientAuthentication::Unsecure {
                client_id: id,
                protocol_id: PROTOCOL_ID,
                server_addr,
                user_data: None,
            };

            let transport =
                NetcodeClientTransport::new(current_time, authentication, socket).unwrap();

            Self {
                client,
                transport,
                stats: Stats::new(),
            }
        }

        fn init_bevy(app: &mut App, id: u64) {
            app.add_plugins(bevy_renet::transport::NetcodeClientPlugin);
            app.add_plugins(ClientPlugin::<RenetClientShim>::default());
            app.configure_sets(Update, Connected.run_if(client_connected::<Self>));

            let shim = Self::init(id);

            app.insert_resource(shim.transport);
            app.insert_resource(shim.client);

            // If any error is found we just panic
            #[allow(clippy::never_loop)]
            fn panic_on_error_system(mut renet_error: EventReader<NetcodeTransportError>) {
                for e in renet_error.read() {
                    panic!("{}", e);
                }
            }

            app.add_systems(Update, panic_on_error_system);

            // app.insert_resource(shim);
        }

        fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<bytes::Bytes> {
            self.client.receive_message(channel)
        }

        fn send_message<I: Into<u8>, B: Into<bytes::Bytes>>(&mut self, channel_id: I, message: B) {
            self.client.send_message(channel_id, message)
        }

        fn update(&mut self, duration: std::time::Duration) {
            self.client.update(duration);
        }

        fn update_transport(&mut self, duration: std::time::Duration) {
            self.transport.update(duration, &mut self.client).unwrap();
        }

        fn send_packets(&mut self) {
            self.transport.send_packets(&mut self.client).unwrap();
        }

        fn network_info(&self) -> bevy_renet::renet::NetworkInfo {
            self.client.network_info()
        }

        fn is_connected(&self) -> bool {
            self.client.is_connected()
        }
    }

    impl HeadlessClient for RenetClientShim {
        async fn new(id: ClientId, addr: impl std::net::ToSocketAddrs) -> Self {
            let client = RenetClient::new(connection_config());
            let server_addr = addr.to_socket_addrs().unwrap().next().unwrap();
            let socket = UdpSocket::bind("127.0.0.1:0").unwrap();

            let current_time = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            let authentication = ClientAuthentication::Unsecure {
                client_id: id.0,
                protocol_id: PROTOCOL_ID,
                server_addr,
                user_data: None,
            };

            let transport =
                NetcodeClientTransport::new(current_time, authentication, socket).unwrap();

            Self {
                client,
                transport,
                stats: Stats::new(),
            }
        }

        async fn receive_message<I: Into<u8>>(&mut self, channel: I) -> Option<bytes::Bytes> {
            if let Some(b) = self.client.receive_message(channel) {
                self.stats.bytes_received(b.len());
                self.stats.received_packet();
                Some(b)
            } else {
                None
            }
        }

        async fn send_message<B: Into<bytes::Bytes>>(&mut self, channel_id: u8, message: B) {
            let message = message.into();
            self.stats.sent_packets(1, message.len());
            self.client.send_message(channel_id, message)
        }

        async fn update(&mut self, duration: std::time::Duration) {
            let rtt = self.client.rtt();
            self.stats.rtt(Duration::from_secs_f64(rtt).as_nanos() as u64);
            self.client.update(duration);
        }

        async fn update_transport(&mut self, duration: std::time::Duration) {
            self.transport.update(duration, &mut self.client).unwrap();
        }

        async fn send_packets(&mut self) {
            self.transport.send_packets(&mut self.client).unwrap();
        }

        fn is_connected(&self) -> bool {
            self.client.is_connected()
        }
    }
}

pub mod server {
    use std::{net::UdpSocket, time::SystemTime};

    use bevy::app::App;
    use bevy_renet::renet::{
        transport::{NetcodeServerTransport, ServerAuthentication, ServerConfig},
        NetworkInfo, RenetServer,
    };

    use crate::{
        connection_config,
        transports::{headless_traits::HeadlessServer, plugins::ServerPlugin, ClientId, Server},
    };

    impl Server for RenetServer {
        fn init_bevy(app: &mut App) {
            use bevy_renet::transport::NetcodeServerPlugin;

            app.add_plugins(NetcodeServerPlugin);
            app.add_plugins(ServerPlugin::<RenetServer>::default());

            let server = RenetServer::new(connection_config());

            let public_addr = "127.0.0.1:5000".parse().unwrap();
            let socket = UdpSocket::bind(public_addr).unwrap();
            let current_time: std::time::Duration = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            let server_config = ServerConfig {
                current_time,
                max_clients: 64,
                protocol_id: super::PROTOCOL_ID,
                public_addresses: vec![public_addr],
                authentication: ServerAuthentication::Unsecure,
            };

            let transport = NetcodeServerTransport::new(server_config, socket).unwrap();
            app.insert_resource(server);
            app.insert_resource(transport);
        }

        fn broadcast_message<I: Into<u8>, B: Into<bytes::Bytes>>(
            &mut self,
            channel_id: I,
            message: B,
        ) {
            self.broadcast_message(channel_id, message)
        }

        fn receive_message<I: Into<u8>>(
            &mut self,
            client_id: crate::transports::ClientId,
            channel_id: I,
        ) -> Option<bytes::Bytes> {
            self.receive_message(client_id.into(), channel_id)
        }

        fn send_message<I: Into<u8>, B: Into<bytes::Bytes>>(
            &mut self,
            client_id: crate::transports::ClientId,
            channel_id: I,
            message: B,
        ) {
            self.send_message(client_id.into(), channel_id, message)
        }

        fn clients_id(&self) -> Vec<crate::transports::ClientId> {
            self.clients_id().into_iter().map(ClientId::from).collect()
        }

        fn update(&mut self, duration: std::time::Duration) {
            self.update(duration);
        }

        fn get_event(&mut self) -> Option<bevy_renet::renet::ServerEvent> {
            self.get_event()
        }

        fn network_info(&self, client_id: crate::transports::ClientId) -> Option<NetworkInfo> {
            self.network_info(client_id.into()).ok()
        }
    }

    pub struct RenetServer2 {
        server: RenetServer,
        transport: NetcodeServerTransport,
    }

    impl HeadlessServer for RenetServer2 {
        async fn new(addr: impl std::net::ToSocketAddrs) -> Self {
            let server = RenetServer::new(connection_config());

            let addr = addr.to_socket_addrs().unwrap().next().unwrap();
            let socket = UdpSocket::bind(addr).unwrap();
            let current_time: std::time::Duration = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap();
            let server_config = ServerConfig {
                current_time,
                max_clients: 64,
                protocol_id: super::PROTOCOL_ID,
                public_addresses: vec![addr],
                authentication: ServerAuthentication::Unsecure,
            };
            let transport = NetcodeServerTransport::new(server_config, socket).unwrap();
            Self { server, transport }
        }

        async fn broadcast_message<B: Into<bytes::Bytes>>(
            &mut self,
            channel_id: u8,
            message: B,
        ) {
            self.server.broadcast_message(channel_id, message);
        }

        async fn receive_message<I: Into<u8>>(
            &mut self,
            client_id: ClientId,
            channel_id: I,
        ) -> Option<bytes::Bytes> {
            self.server.receive_message(client_id.into(), channel_id)
        }

        async fn send_message<B: Into<bytes::Bytes>>(
            &mut self,
            client_id: ClientId,
            channel_id: u8,
            message: B,
        ) {
            self.server
                .send_message(client_id.into(), channel_id, message);
        }

        fn client_ids(&self) -> Vec<ClientId> {
            self.server
                .clients_id()
                .into_iter()
                .map(ClientId::from)
                .collect()
        }

        async fn update(&mut self, duration: std::time::Duration) {
            self.server.update(duration);
            self.transport.update(duration, &mut self.server).unwrap();
        }

        fn get_event(&mut self) -> Option<bevy_renet::renet::ServerEvent> {
            self.server.get_event()
        }
    }
}
