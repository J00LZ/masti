use std::fs::{File, OpenOptions};
use std::time::{Duration, Instant};

use rust_raknet::RaknetSocket;
#[cfg(feature = "quic")]
use s2n_quic::stream::BidirectionalStream;
use sctp_rs::ConnectedSocket;
use serde::{Deserialize, Serialize};
use tokio::net::TcpStream;
use tokio::task::JoinSet;
use tokio::time::sleep;

use crate::args::{RunnerArgs, TransportType};
use crate::client::impls::ClientImpl;
use crate::types::{Message, PlayerCommand, PlayerInput};

use self::impls::ClientTransport;
use self::raknet::{Reliable, Unreliable};

pub mod dccp;
pub mod impls;
#[cfg(feature = "quic")]
pub mod quic;
pub mod raknet;
pub mod sctp;
pub mod tcp;

pub async fn run_client(args: RunnerArgs) {
    let transport = args.transport;
    match transport {
        TransportType::Tcp => run_client_impl::<TcpStream>(args).await,
        #[cfg(feature = "quic")]
        TransportType::Quic => run_client_impl::<BidirectionalStream>(args).await,
        #[cfg(not(feature = "quic"))]
        TransportType::Quic => panic!("Quic not enabled"),
        TransportType::Sctp => run_client_impl::<ConnectedSocket>(args).await,
        TransportType::RakNetReliable => run_client_impl::<(RaknetSocket, Reliable)>(args).await,
        TransportType::RakNetUnreliable => {
            run_client_impl::<(RaknetSocket, Unreliable)>(args).await
        }
        TransportType::DCCP => run_client_impl::<::dccp::DCCPSocket>(args).await,
    }
}

async fn run_client_impl<C: ClientTransport + 'static>(args: RunnerArgs) {
    // Read log file
    let file = File::open(&args.log).unwrap();
    let log = args.log.clone();
    let mut inputs: Vec<Bar> = serde_json::from_reader(file).unwrap();
    inputs.reverse(); // Newest at the back so we can pop
                      // remove the last input if it'll take too long to get there
    if inputs[0].time > Duration::from_secs(100) {
        inputs.remove(0);
    }
    inputs.reverse();

    let loops = args.loops;
    let communication_type = args.communication_type.to_string();
    // Initialize tcp client
    // let mut client = SelectedClient::init(client_id); // tcp client gets id from server
    // let mut c = HC::<TcpClient>::new(ClientId(client_id), "127.0.0.1:5000").await;
    let clients = ClientImpl::<C>::start(args).await;
    let mut set = JoinSet::new();
    for client in clients {
        let inputs = inputs.clone();
        let communication_type = communication_type.clone();
        let log = log.to_str().unwrap().to_string();
        set.spawn(async move {
            let client = ClientImpl::with_connection(client);
            println!("Connected!");

            client.stats.set_should_send(inputs.len() * loops);
            client
                .stats
                .set_communication_type(communication_type.clone());
            client.stats.set_log_name(format!("{}", log));

            for _ in 0..loops {
                // println!("Loop {}", i);
                // Fake game loop
                run_inputs(&client, inputs.clone()).await;
            }

            let stats = client.stats;
            stats
        });
    }
    while let Some(Ok(stats)) = set.join_next().await {
        println!("Stats: {:?}", stats);
        let mut f = OpenOptions::new()
            .append(true)
            .create(true)
            .open("./out.csv")
            .unwrap();
        stats.write_all(&mut f);
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub enum Foo {
    PlayerInput(PlayerInput),
    PlayerCommand(PlayerCommand),
}

impl From<Foo> for Message {
    fn from(value: Foo) -> Self {
        match value {
            Foo::PlayerInput(p) => Message::Input(p),
            Foo::PlayerCommand(p) => Message::Command(p),
        }
    }
}

#[derive(Serialize, Deserialize, Clone)]
pub struct Bar {
    pub foo: Foo,
    pub time: Duration,
}

async fn run_inputs<C: 'static + ClientTransport>(client: &ClientImpl<C>, inputs: Vec<Bar>) {
    let frame_duration = Duration::from_secs_f32(1.0 / 60.0);
    let mut inputs = inputs.into_iter().peekable();
    let mut last_time = Instant::now();
    let start_time = Instant::now();

    loop {
        let elapsed = last_time.elapsed();
        if elapsed < frame_duration {
            // tokio::time::sleep(frame_duration - elapsed).await;
        } else {
            eprintln!("Could not keep up")
        }
        last_time = Instant::now();
        client.update(elapsed);
        // client connected
        if client.is_connected() {
            while let Some(_m) = client.receive_message() {
                // println!("Received message: {m:?}");
            }
            while let Some(i) = inputs.peek() {
                if start_time.elapsed() >= i.time {
                    let i = inputs.next().unwrap();
                    client.send_message(i.foo.into());
                } else {
                    break;
                }
            }
        }

        if inputs.peek().is_none() {
            sleep(Duration::from_millis(250)).await;
            break;
        }

        sleep(Duration::from_millis(1)).await;
    }
}
