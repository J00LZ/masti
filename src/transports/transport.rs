use std::collections::{HashMap, VecDeque};
use std::future::Future;
use std::io::Result as IoResult;
use std::marker::PhantomData;
use std::pin::pin;
use std::sync::atomic::{AtomicBool, AtomicU64, Ordering};
use std::sync::mpsc::{channel, Receiver, Sender, TryRecvError};
use std::sync::Arc;
use std::task::Poll;
use std::time::Duration;

use bytes::{Bytes, BytesMut};
use futures::poll;
use postcard::accumulator::CobsAccumulator;
use tokio::io::{AsyncReadExt, AsyncWriteExt};
use tokio::net::{TcpListener, TcpStream};

use crate::transports::stats::Stats;
use crate::transports::ClientId;
use crate::{Message, MessageWrapper, ServerEvent};

pub trait ClientTransport: Sized + Send {
    type Server;

    fn accept(server: &mut Self::Server) -> impl Future<Output = Option<Self>> + Send;

    fn new(ip: &str) -> impl Future<Output = Self> + Send;

    fn write_message(&mut self, bytes: Bytes) -> impl Future<Output = IoResult<()>> + Send;

    fn read_bytes(&mut self) -> impl Future<Output = IoResult<Bytes>> + Send;
}

pub struct ClientImpl<T> {
    _client: PhantomData<T>,
    recv_buffer: Receiver<Message>,
    send_buffer: Sender<Message>,
    now: Arc<AtomicU64>, // In Nanoseconds
    pub stats: Stats,
    connected: Arc<AtomicBool>,
}

impl<T: 'static + ClientTransport> ClientImpl<T> {
    pub async fn start(addr: &str) -> Self {
        let addr = addr.to_string();
        let client = T::new(&addr).await;
        Self::with_connection(client)
    }

    fn with_connection(t: T) -> Self {
        let (recv_input, recv_buffer) = channel();
        let (send_buffer, send_output) = channel();

        let now = Arc::new(AtomicU64::new(0));
        let now_clone = now.clone();
        let send_buffer_clone = send_buffer.clone();
        let connected = Arc::new(AtomicBool::new(true));
        let bool2 = connected.clone();
        let stats = Stats::new();
        let stats2 = stats.clone();
        tokio::task::spawn(async move {
            let mut client = t;
            let mut buffer = BytesMut::new();
            let mut cobs_buf = CobsAccumulator::<1536>::new();
            let now = now_clone;
            let send_buffer = send_buffer_clone;
            let connected = bool2;
            let stats = stats2;

            'outer: loop {
                match send_output.try_recv() {
                    Ok(m) => {
                        let is_ack = matches!(m, Message::Ack(_));
                        let time = now.load(Ordering::SeqCst);
                        let msg = MessageWrapper(m, time);
                        // println!("Sent message: {msg:?}");
                        let msg = postcard::to_allocvec_cobs(&msg).unwrap();
                        if !is_ack {
                            stats.sent_packets(1, msg.len());
                        }
                        if let Err(e) = client.write_message(Bytes::from(msg)).await {
                            eprintln!("{e}");
                            connected.store(false, Ordering::SeqCst);
                            break;
                        }
                    }
                    Err(TryRecvError::Empty) => {}
                    Err(TryRecvError::Disconnected) => {
                        connected.store(false, Ordering::SeqCst);
                        break;
                    }
                }

                match poll!(pin!(client.read_bytes())) {
                    Poll::Ready(msg) => {
                        if let Ok(msg) = msg {
                            buffer.extend_from_slice(&msg);
                            stats.bytes_received(msg.len());
                        } else {
                            connected.store(false, Ordering::SeqCst);
                            break;
                        }
                    }
                    Poll::Pending => {}
                }
                let mut buf = &buffer[..];
                let remaining: &[u8] = 'cobs: loop {
                    match cobs_buf.feed::<MessageWrapper>(buf) {
                        postcard::accumulator::FeedResult::Consumed => break 'cobs &[],
                        postcard::accumulator::FeedResult::OverFull(f) => {
                            break 'cobs f;
                        }
                        postcard::accumulator::FeedResult::DeserError(f) => {
                            break 'cobs f;
                        }
                        postcard::accumulator::FeedResult::Success {
                            data: msg,
                            remaining,
                        } => {
                            let MessageWrapper(msg, send_time) = msg;
                            if let Message::Ack(time) = msg {
                                let t = now.load(Ordering::SeqCst);
                                let diff = t - time;
                                stats.rtt(diff);
                            } else {
                                stats.received_packet();
                                if let Err(e) = recv_input.send(msg) {
                                    eprintln!("{e}");
                                    connected.store(false, Ordering::SeqCst);
                                    break 'outer;
                                }
                                let ack = Message::Ack(send_time);
                                if let Err(e) = send_buffer.send(ack) {
                                    eprintln!("{e}");
                                    connected.store(false, Ordering::SeqCst);
                                    break 'outer;
                                }
                            }

                            buf = remaining;
                        }
                    }
                };
                buffer = bytes::BytesMut::from(remaining);
                tokio::task::yield_now().await;
            }
        });

        Self {
            _client: PhantomData,
            recv_buffer,
            send_buffer,
            now,
            stats,
            connected,
        }
    }

    pub fn receive_message(&self) -> Option<Message> {
        self.recv_buffer.try_recv().ok()
    }

    pub fn send_message(&self, message: Message) {
        self.send_buffer.send(message).unwrap()
    }

    pub fn update(&self, duration: Duration) {
        self.now
            .fetch_add(duration.as_nanos() as u64, Ordering::SeqCst);
    }

    pub fn is_connected(&self) -> bool {
        self.connected.load(Ordering::SeqCst)
    }
}

impl ClientTransport for TcpStream {
    type Server = TcpListener;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        match poll!(pin!(server.accept())) {
            Poll::Ready(c) => {
                let (stream, _) = c.ok()?;
                Some(stream)
            }
            Poll::Pending => None,
        }
    }

    async fn new(ip: &str) -> Self {
        TcpStream::connect(ip).await.unwrap()
    }

    async fn write_message(&mut self, bytes: Bytes) -> IoResult<()> {
        self.write_all(&bytes).await
    }

    async fn read_bytes(&mut self) -> IoResult<Bytes> {
        let mut bytes = [0; 1500];
        let r = self.read(&mut bytes).await?;
        Ok(Bytes::copy_from_slice(&bytes[..r]))
    }
}

pub trait ServerTransport: Sized + Send {
    const NAME: &'static str;
    fn new(addr: &str) -> impl Future<Output = Self>;
}

pub struct ServerImpl<C: ClientTransport> {
    server: C::Server,
    clients: HashMap<ClientId, ClientImpl<C>>,
    events: VecDeque<ServerEvent>,
}

impl<S: ServerTransport, C: 'static + ClientTransport<Server = S>> ServerImpl<C> {
    pub async fn new(addr: &str) -> Self {
        let server = S::new(addr).await;

        Self {
            server,
            clients: Default::default(),
            events: Default::default(),
        }
    }

    pub async fn update(&mut self, time: Duration) {
        if let Some(c) = C::accept(&mut self.server).await {
            let id = ClientId::new();
            self.clients.insert(id, ClientImpl::with_connection(c));
            self.events.push_back(ServerEvent::ClientConnected(id));
        }

        for client in self.clients.values() {
            client.update(time);
        }

        self.clients.retain(|id, client| {
            if !client.is_connected() {
                self.events.push_back(ServerEvent::ClientDisconnected(*id));
                false
            } else {
                true
            }
        });
    }

    pub async fn broadcast(&self, message: Message) {
        for client in self.clients.values() {
            client.send_message(message.clone());
        }
    }

    pub async fn send_message(&self, client_id: ClientId, message: Message) {
        if let Some(c) = self.clients.get(&client_id) {
            c.send_message(message);
        }
    }

    pub async fn receive_message(&self, client_id: ClientId) -> Option<Message> {
        self.clients
            .get(&client_id)
            .and_then(|c| c.receive_message())
    }

    pub fn client_ids(&self) -> impl Iterator<Item = ClientId> + '_ {
        self.clients.keys().copied()
    }

    pub fn get_event(&mut self) -> Option<ServerEvent> {
        self.events.pop_front()
    }
}

impl ServerTransport for TcpListener {
    const NAME: &'static str = "TCP";

    async fn new(addr: &str) -> Self {
        TcpListener::bind(addr).await.unwrap()
    }
}
