use std::{
    collections::{HashMap, VecDeque},
    future::Future,
    time::Duration,
};

use crate::{
    args::RunnerArgs, client::impls::{ClientImpl, ClientTransport}, transports::ClientId, types::{Message, ServerEvent}
};

pub trait ServerTransport: Sized + Send {
    fn new(args: RunnerArgs) -> impl Future<Output = Self>;

    fn update(&mut self, _time: Duration) -> impl Future<Output = ()> {
        async {}
    }
}

pub struct ServerImpl<C: ClientTransport> {
    server: C::Server,
    clients: HashMap<ClientId, ClientImpl<C>>,
    events: VecDeque<ServerEvent>,
}

impl<S: ServerTransport, C: 'static + ClientTransport<Server = S>> ServerImpl<C> {
    pub async fn new(args: RunnerArgs) -> Self {
        let server = S::new(args).await;

        Self {
            server,
            clients: Default::default(),
            events: Default::default(),
        }
    }

    pub async fn update(&mut self, time: Duration) {
        self.server.update(time).await;

        if let Some(c) = C::accept(&mut self.server).await {
            let id = ClientId::new();
            self.clients.insert(id, ClientImpl::with_connection(c));
            self.events.push_back(ServerEvent::ClientConnected(id));
        }

        for client in self.clients.values() {
            client.update(time);
        }

        self.clients.retain(|id, client| {
            if !client.is_connected() {
                self.events.push_back(ServerEvent::ClientDisconnected(*id));
                false
            } else {
                true
            }
        });
    }

    pub async fn broadcast(&self, message: Message) {
        for client in self.clients.values() {
            client.send_message(message.clone());
        }
    }

    pub async fn send_message(&self, client_id: ClientId, message: Message) {
        if let Some(c) = self.clients.get(&client_id) {
            c.send_message(message);
        }
    }

    pub async fn receive_message(&self, client_id: ClientId) -> Option<Message> {
        self.clients
            .get(&client_id)
            .and_then(|c| c.receive_message())
    }

    pub fn client_ids(&self) -> impl Iterator<Item = ClientId> + '_ {
        self.clients.keys().copied()
    }

    pub fn get_event(&mut self) -> Option<ServerEvent> {
        self.events.pop_front()
    }
}
