use clap::Parser;

#[tokio::main]
async fn main() {
    let args = runner::args::RunnerArgs::parse();
    println!("{:?}", args);
    match args.side {
        runner::args::Side::Client => {
            runner::client::run_client(args).await;
        }
        runner::args::Side::Server => {
            // console_subscriber::init();
            runner::server::run_server(args).await;
        }
    }
}
