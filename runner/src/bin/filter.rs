use std::{fs::File, path::PathBuf, str::FromStr};

use runner::client::Bar;

fn main() {
    let path = PathBuf::from_str(&std::env::args().skip(1).next().unwrap()).unwrap();
    let file = File::open(&path).unwrap();
    let inputs: Vec<Bar> = serde_json::from_reader(file).unwrap();
    let inputs23 = inputs
        .iter()
        .cloned()
        .enumerate()
        .filter_map(|(idx, elem)| if idx % 3 > 0 { Some(elem) } else { None })
        .collect::<Vec<_>>();

    let inputs13 = inputs
        .iter()
        .cloned()
        .enumerate()
        .filter_map(|(idx, elem)| if idx % 3 == 0 { Some(elem) } else { None })
        .collect::<Vec<_>>();
    println!(
        "{} total, 2/3 = {}, 1/3 = {}",
        inputs.len(),
        inputs23.len(),
        inputs13.len()
    );

    let file_name = path.file_stem().unwrap();
    let p23 = path.with_file_name(format!("{}-2-3.json", file_name.to_str().unwrap()));
    let p13 = path.with_file_name(format!("{}-1-3.json", file_name.to_str().unwrap()));

    let mut f23 = File::create(&p23).unwrap();
    serde_json::to_writer(&mut f23, &inputs23).unwrap();

    let mut f13 = File::create(&p13).unwrap();
    serde_json::to_writer(&mut f13, &inputs13).unwrap();
}
