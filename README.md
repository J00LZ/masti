# MASTI
Originally based on https://github.com/lucaspoffo/renet, but since it has changed so much that's not really relevant anymore. 

## Running our project
To run the project you need [Rust](https://rust-lang.org). The project itself is contained in the `runner` direcory. The `src` directory contains a lot of our old experiments, check the `tcp.rs` file to see some iterations. 

When rust is installed you can run the project using `cargo run`. This will show you that you need to enter two options, which side you want to run, and what protocol you want to use. 

There are also various command line options, most importantly `--address/-a` which controls the address the server listens to, and the client tries to connect to. 

### Our arguments
The server was always run like this: 
```sh
$ ./runner server $PROTO --address 0.0.0.0:5000
```
Binding it to any address, and listening to port 5000

The client was run like this: 
```sh
$  ./target/release/runner client $PROTO -c ethernet -a $SERVER_IP:5000 --clients 2 --loops 2 -l ./logs/42.json
```
Where the last parameter is changed depending on if we wanted to run the full, the medium or the small version. 

## `pcap-analyse`
This program reads the `pcap` files produced by Wireshark, and converts them to a simple CSV for us to use. This CSV is output to stdout so it's easy to pipe it from one program to another. 

## `csv-merge`
Merges the output of the various clients together, either adding or combining the data. Since we ran all the experiments with two clients, we had double the data used and double the statistics. 

