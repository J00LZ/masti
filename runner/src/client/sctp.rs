use std::pin::pin;

use bytes::Bytes;
use futures::poll;
use sctp_rs::{ConnectedSocket, SendData, Socket};

use crate::args::RunnerArgs;

use super::impls::ClientTransport;

impl ClientTransport for ConnectedSocket {
    const NAME: &'static str = "sctp";

    type Server = sctp_rs::Listener;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        match poll!(pin!(server.accept())) {
            std::task::Poll::Ready(c) => {
                let (a, _) = c.unwrap();
                Some(a)
            }
            std::task::Poll::Pending => None,
        }
    }

    async fn new(args: RunnerArgs) -> Self {
        let r = Socket::new_v4(sctp_rs::SocketToAssociation::OneToOne).unwrap();
        let (s, _) = r.connect(args.address).await.unwrap();

        s
    }

    async fn write_message(&mut self, bytes: bytes::Bytes) -> std::io::Result<()> {
        self.sctp_send(SendData {
            payload: bytes.to_vec(),
            snd_info: None,
        })
        .await?;
        Ok(())
    }

    async fn read_bytes(&mut self) -> std::io::Result<bytes::Bytes> {
        let r = self.sctp_recv().await?;
        match r {
            sctp_rs::NotificationOrData::Notification(n) => {
                println!("Notification: {:?}", n);
                Ok(Bytes::new())
            }
            sctp_rs::NotificationOrData::Data(d) => {
                let data = d.payload;
                Ok(data.into())
            }
        }
    }
}
