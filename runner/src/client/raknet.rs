use std::pin::pin;

use futures::poll;
use rust_raknet::{RaknetListener, RaknetSocket};

use crate::args::RunnerArgs;

use super::impls::ClientTransport;

pub trait RakNetReliability {
    const NAME: &'static str;

    fn to_raknet() -> rust_raknet::Reliability;
}

#[derive(Default)]
pub struct Unreliable;

impl RakNetReliability for Unreliable {
    const NAME: &'static str = "raknet_unreliable";
    fn to_raknet() -> rust_raknet::Reliability {
        rust_raknet::Reliability::Unreliable
    }
}

#[derive(Default)]
pub struct Reliable;

impl RakNetReliability for Reliable {
    const NAME: &'static str = "raknet_reliable";
    fn to_raknet() -> rust_raknet::Reliability {
        rust_raknet::Reliability::ReliableSequenced
    }
}

impl<R: RakNetReliability + Send + Default> ClientTransport for (RaknetSocket, R) {
    const NAME: &'static str = R::NAME;

    type Server = RaknetListener;

    async fn accept(server: &mut Self::Server) -> Option<Self> {
        match poll!(pin!(server.accept())) {
            std::task::Poll::Ready(r) => Some((r.ok()?, R::default())),
            std::task::Poll::Pending => None,
        }
    }

    async fn new(args: RunnerArgs) -> Self {
        println!("Connecting to server");
        let socket = RaknetSocket::connect(&args.address).await.unwrap();
        println!("Connected to server");
        (socket, R::default())
    }

    async fn write_message(&mut self, bytes: bytes::Bytes) -> std::io::Result<()> {
        let mut data = bytes.to_vec();
        data.insert(0, 0xfe);
        self.0.send(&data, R::to_raknet()).await.map_err(|e| {
            std::io::Error::new(std::io::ErrorKind::Other, format!("send error: {:?}", e))
        })?;
        Ok(())
    }

    async fn read_bytes(&mut self) -> std::io::Result<bytes::Bytes> {
        self.0
            .recv()
            .await
            .map(|b| {
                if b[0] == 0xfe {
                    bytes::Bytes::copy_from_slice(&b[1..])
                } else {
                    b.into()
                }
            })
            .map_err(|e| {
                std::io::Error::new(std::io::ErrorKind::Other, format!("recv error: {:?}", e))
            })
    }
}
